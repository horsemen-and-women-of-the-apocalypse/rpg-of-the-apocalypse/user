/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { ICollection, IReadOnlyCollection } from "./database/collection";
import { Document, Filter, ObjectId, UpdateResult } from "mongodb";
import { User, UserCreationInfo } from "../model/user";
import { AuthInfo } from "../model/auth_info";

/**
 * Interface for a service exposing methods related to the database with a read-only access
 */
interface IReadOnlyDatabaseService {
    /**
     * Ping the database
     */
    ping(): Promise<void>;
}

/**
 * Interface for a service exposing methods related to the database
 */
interface IDatabaseService extends IReadOnlyDatabaseService {
    /**
     * Get the collection with the given name
     *
     * @param {string} name Collection's name
     */
    collection(name: string): Promise<ICollection>;

    /**
     * Drop all collections
     */
    dropCollections(): Promise<void>;
}

/**
 * Interface for a service exposing methods related to {@link User}s in the database
 */
interface IUserDatabaseService {
    /**
     * Get all users
     */
    all(): Promise<User[]>;

    /**
     * Find users matching the given filter while skipping <code>offset</code> users and
     * limiting the number of results to <code>limit</code>
     *
     * @param {Filter<Document>} filter Filter to respect
     * @param {number} offset Number of users to skip
     * @param {limit} limit Maximum number of users to return (0 <=> no limit)
     */
    find(filter: Filter<Document>, offset: number, limit: number): Promise<User[]>;

    /**
     * Find the user with the given identifier
     *
     * @param {ObjectId} id User's identifier
     * @return {Promise<User | null>} The user if there is one, otherwise null
     */
    findById(id: ObjectId): Promise<User | null>;

    /**
     * Find the user with the given username
     *
     * @param {string} username User's username
     * @return {Promise<User | null>} The user if there is one, otherwise null
     */
    findByUsername(username: string): Promise<User | null>;

    /**
     * Insert the given user in the database
     *
     * @param {UserCreationInfo} user Information required to create a user
     * @return {Promise<ObjectId>} User's identifier
     */
    insert(user: UserCreationInfo): Promise<ObjectId>;

    /**
     * Update authentication information for the user with the given identifier or username
     *
     * @param {ObjectId} id User's identifier or username
     * @param {AuthInfo} authInfo Updated auth information
     */
    updateAuthInfo(id: ObjectId | string, authInfo: AuthInfo): Promise<UpdateResult>;

    /**
     * Remove the user with the given identifier or username
     *
     * @param {ObjectId|string} id Identifier or username
     * @return {Promise<boolean>} true if the user was removed, false otherwise
     */
    remove(id: ObjectId | string): Promise<boolean>;

    /**
     * Get the collection used to store {@link User}s
     */
    collection(): Promise<IReadOnlyCollection>;
}

/**
 * Interface for a service exposing methods related to {@link User}s in the application
 */
interface IUserService {
    /**
     * Create a user with the given username with a random password
     *
     * @param {string} username Username
     * @return {Promise<string>} Clear password
     */
    // username can be anything
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    create(username: any): Promise<string>;

    /**
     * Remove a user based on its username
     *
     * @param {string} username Username
     * @return {Promise<boolean>} true if the user was removed, false otherwise
     */
    // username can be anything
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    remove(username: any): Promise<boolean>;

    /**
     * List users having the given username/role while skipping <code>offset</code> users and
     * limiting the number of results to <code>limit</code>. If a parameter is undefined or null, it will
     * be ignored in the filter of results
     *
     * @param username Username to match
     * @param role Role to match
     * @param offset Number of users to skip
     * @param limit Maximum number of users to return
     */
    // parameters can be anything
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    list(username: any, role: any, offset: any, limit: any): Promise<User[]>;

    /**
     * Find the user with the given username
     *
     * @param username Username
     * @return {Promise<User | null>} User or null if it doesn't exist
     */
    // username can be anything
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    find(username: any): Promise<User | null>;
}

/**
 * Interface for a service exposing methods related to the hashing/comparison of passwords
 */
interface IPasswordHashService {
    /**
     * Hash the given password
     *
     * @param {string} password Password to hash
     * @return {Promise<string>} Hash
     */
    hash(password: string): Promise<string>;

    /**
     * Test whether the given password corresponds to the specified hash
     *
     * @param {string} password Clear password
     * @param {string} hash Hash
     * @return {Promise<boolean>} true if the password corresponds to the hash, false otherwise
     */
    areEqual(password: string, hash: string): Promise<boolean>;
}

/**
 * Service exposing methods related to the version of the application
 */
interface IVersionService {
    /**
     * Version of the application (format: {@link VersionService#VERSION_FORMAT_DESCRIPTION}) or undefined if it can't be deduced
     */
    get version(): string | undefined;
}

/**
 * Service exposing methods related to message broking
 */
interface IMessageBroker {
    /**
     * Ping the rabbitmq instance
     */
    ping(): Promise<void>;


    /**
     * Notify that the given user was removed
     *
     * @param {string} username User's username
     */
    notifyUserRemoval(username: string): Promise<void>;
}

/**
 * Interface for a service exposing methods related to {@link User} {@link AuthInfo}s in the database
 */
interface IAuthService {
    /**
     * Generate synchronously a token for a given username, based on set configuration
     *
     * @param {string} username Username used to fill subject claim
     * @param {string} expiresIn Expiration delay of the token expressed in seconds
     * or a string describing a time span zeit/ms (by default, use config value)
     * @return {string} Generated token
     */
    tokenSync(username: string, expiresIn?: string): string;

    /**
     * Retrieve subject claim value in token
     *
     * @param {string} token Token to extract
     * @return {string} Subject value
     */
    getSubject(token: string): string;

    /**
     * Verify a given token
     * Method name is different to prevent confusion with jwt.verify method
     *
     * @param token Token to check
     * @return {Promise<boolean>} true if the token is valid, false otherwise
     */
    // token can be anything
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    check(token: any): Promise<boolean>;
}

export {
    IReadOnlyDatabaseService,
    IDatabaseService,
    IUserDatabaseService,
    IAuthService,
    IUserService,
    IPasswordHashService,
    IVersionService,
    IMessageBroker
};
