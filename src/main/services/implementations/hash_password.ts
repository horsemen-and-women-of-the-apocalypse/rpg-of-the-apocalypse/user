/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { IPasswordHashService } from "../interfaces";
import { injectable } from "inversify";
import bcrypt from "bcrypt";
import config from "../../config/service";

const SALT = config.userAuth.salt;

/**
 * Implementation of {@link IPasswordHashService} based on bcrypt
 */
@injectable()
class PasswordHashService implements IPasswordHashService {
    hash(password: string): Promise<string> {
        PasswordHashService.check(password);

        return bcrypt.hash(password, SALT);
    }

    areEqual(password: string, hash: string): Promise<boolean> {
        return bcrypt.compare(password, hash);
    }

    /**
     * Hash synchronously the given password
     *
     * @param {string} password Password to hash
     * @return {string} Hash of the password
     */
    public hashSync(password: string): string {
        PasswordHashService.check(password);

        return bcrypt.hashSync(password, SALT);
    }

    /**
     * Check if the given password can be hashed
     *
     * @param {string} password Password to check
     * @private
     */
    private static check(password: string) {
        // Check password length (See https://github.com/kelektiv/node.bcrypt.js#security-issues-and-concerns)
        const bytes = Buffer.byteLength(password);
        if (bytes > 72) {
            throw new Error(`Password is too long (size in bytes: ${bytes}). It should be less than or equal to 72 bytes`);
        }
    }
}

export default PasswordHashService;
