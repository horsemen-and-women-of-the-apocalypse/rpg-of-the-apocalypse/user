/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { IVersionService } from "../interfaces";
import { injectable } from "inversify";

/**
 * Implementation of {@link IVersionService} based on the package.json
 */
@injectable()
class VersionService implements IVersionService {
    static readonly VERSION_FORMAT_DESCRIPTION = "{major}.{minor}.{patch} or {major}.{minor}.{patch}-{label}";

    get version(): string | undefined {
        return process.env.npm_package_version;
    }
}

export default VersionService;
