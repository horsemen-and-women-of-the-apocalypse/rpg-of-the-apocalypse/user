/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { inject, injectable } from "inversify";
import { DATABASE, PASSWORD_HASH } from "../instances";
import { IDatabaseService, IPasswordHashService, IUserDatabaseService } from "../interfaces";
import { User, UserCreationInfo, UserField } from "../../model/user";
import {
    Document,
    Filter,
    MongoServerError,
    ObjectId,
    UpdateResult,
    Sort
} from "mongodb";
import { ICollection, IReadOnlyCollection } from "../database/collection";
import LOGGER from "../../logger";
import { UserModelError } from "../../model/user_error";
import { AuthInfo } from "../../model/auth_info";
import { UserRole } from "../../model/role";
import config from "../../config/service";
import { LazyValue } from "../../utils/lazy_value";

// Ensure that the order of results is stable for tests
const SORT: Sort = process.env.NODE_ENV === "production" ? {} : { [UserField.USERNAME]: 1 };

/**
 * Service exposing the operations related to {@link User}s with the database
 */
@injectable()
class UserDatabaseService implements IUserDatabaseService {
    public static readonly ADMIN_USERNAME = "admin";
    public static readonly COLLECTION = "users";

    private static readonly UNIQUE_INDICES = [ UserField.USERNAME ];

    private readonly databaseService: IDatabaseService;
    private readonly hash: IPasswordHashService;

    private readonly collec: LazyValue<Promise<ICollection>>;

    /**
     * Constructor
     *
     * @param {IDatabaseService} databaseService Database service to use
     * @param {IPasswordHashService} hash Password hashing service to use
     */
    constructor(@inject(DATABASE) databaseService: IDatabaseService, @inject(PASSWORD_HASH) hash: IPasswordHashService) {
        this.databaseService = databaseService;
        this.hash = hash;

        this.collec = new LazyValue<Promise<ICollection>>(() => this.initialize());
    }

    async all(): Promise<User[]> {
        return this.find({}, 0, 0);
    }

    async find(filter: Filter<Document>, offset: number, limit: number): Promise<User[]> {
        const users = [];

        for await (const user of (await this.collec.get()).find(filter).sort(SORT).skip(offset).limit(limit)) {
            try {
                users.push(User.fromJSON(user));
            } catch (e) {
                // Ignore malformed users
                if (e instanceof UserModelError) {
                    LOGGER.error(e);
                } else {
                    throw e;
                }
            }
        }

        return users;
    }

    async findById(id: ObjectId): Promise<User | null> {
        const doc = await (await this.collec.get()).findById(id);

        return doc !== null ? User.fromJSON(doc) : null;
    }

    async findByUsername(username: string): Promise<User | null> {
        const doc = await (await this.collec.get()).findOne({
            [UserField.USERNAME]: username
        });

        return doc !== null ? User.fromJSON(doc) : null;
    }

    async insert(user: UserCreationInfo): Promise<ObjectId> {
        return (await this.collec.get()).insert({
            [UserField.AUTH_INFO]: null,
            ...user.toJSON()
        });
    }

    async updateAuthInfo(id: ObjectId | string, authInfo: AuthInfo): Promise<UpdateResult> {
        const collection = await this.collec.get();

        if (id instanceof ObjectId) {
            return collection.updateOneById(id, { [UserField.AUTH_INFO]: authInfo });
        } else {
            const filter: Filter<Document> = {};
            filter[UserField.USERNAME] = id;

            return collection.updateOne(filter, { [UserField.AUTH_INFO]: authInfo });
        }
    }

    async remove(id: ObjectId | string): Promise<boolean> {
        const collection = await this.collec.get();

        if (id instanceof ObjectId) {
            return collection.removeOneById(id);
        } else {
            return collection.removeOne({
                [UserField.USERNAME]: id
            });
        }
    }

    collection(): Promise<IReadOnlyCollection> {
        return this.collec.get();
    }

    /**
     * Initialize the collection with its indices and create the administrator user
     */
    private async initialize(): Promise<ICollection> {
        // Create collection + indices
        const c = await this.databaseService.collection(UserDatabaseService.COLLECTION);
        await c.createIndices(UserDatabaseService.UNIQUE_INDICES, { unique: true });

        // Create admin user
        try {
            await c.insert({
                ...UserCreationInfo.fromFields(
                    UserDatabaseService.ADMIN_USERNAME, UserRole[UserRole.ADMIN], await this.hash.hash(config.userAuth.adminPassword)
                ).toJSON(),
                [UserField.AUTH_INFO]: null
            });
            LOGGER.info("Administrator created");
        } catch (e) {
            if (!(e instanceof MongoServerError)) {
                throw e;
            }

            const eAsMongoError = e as MongoServerError;
            if (eAsMongoError.code !== 11000) {
                throw e;
            }
            LOGGER.warn("Administrator already exists");
        }

        return c;
    }
}

export default UserDatabaseService;
