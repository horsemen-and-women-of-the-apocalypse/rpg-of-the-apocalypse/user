/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { inject, injectable } from "inversify";
import { IAuthService, IUserDatabaseService } from "../interfaces";
import { ApiError } from "@rpga/common";
import config from "../../config/service";
import EmptyAuthInfoBuilder from "../../model/auth_info_builder";
import { sign, TokenExpiredError } from "jsonwebtoken";
import { verify } from "jsonwebtoken";
import { USER_DATABASE } from "../instances";

const PRIVATE_KEY = config.userAuth.privateKey;
const PUBLIC_KEY = config.userAuth.publicKey;
const ISSUER = config.userAuth.issuer;
const EXPIRES_IN = config.userAuth.expiresIn;
const ALGORITHM = config.userAuth.algorithm;

/**
 * Service exposing methods related to {@link AuthInfo}
 */
@injectable()
class AuthService implements IAuthService {
    private readonly databaseService: IUserDatabaseService;

    /**
     * Constructor
     *
     * @param {IUserDatabaseService} databaseService User database service to use
     */
    constructor(@inject(USER_DATABASE) databaseService: IUserDatabaseService) {
        this.databaseService = databaseService;
    }

    tokenSync(username: string, expiresIn: string = EXPIRES_IN): string {
        return sign(
            {},
            PRIVATE_KEY,
            {
                algorithm: ALGORITHM,
                issuer: ISSUER,
                subject: username,
                expiresIn: expiresIn
            }
        );
    }

    getSubject(token: string): string {
        const decoded = verify(token, PUBLIC_KEY, {
            algorithms: [ ALGORITHM ],
        });

        return decoded.sub?.toString() ?? "";
    }

    // token can be anything
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    async check(token: any): Promise<boolean> {
        if (!token)
            throw new AuthServiceError(AuthServiceErrorReason.AUTH_NO_TOKEN);

        const authInfo = new EmptyAuthInfoBuilder().setToken(token).build();

        // Retrieve username and check if token is expired
        let username = "";
        try {
            username = this.getSubject(authInfo.token);
        } catch(error) {
            if (error instanceof TokenExpiredError)
                throw new AuthServiceError(AuthServiceErrorReason.AUTH_EXPIRED_TOKEN);
        }

        // Check if user exists and retrieve him to compare his token with the given one
        const user = await this.databaseService.findByUsername(username);

        if (!user || !user.isAuthInfoTokenEqual(token))
            throw new AuthServiceError(AuthServiceErrorReason.AUTH_UNKNOWN_TOKEN);

        return true;
    }
}

/**
 * Error related to {@link AuthService} to be sent to client
 */
class AuthServiceError extends ApiError {
    constructor(reason: AuthServiceErrorReason) {
        super(AuthServiceErrorReason[reason], 500);
    }
}

/**
 * Reasons of error in {@link AuthService}
 */
enum AuthServiceErrorReason {
    AUTH_NO_TOKEN,
    AUTH_UNKNOWN_TOKEN,
    AUTH_EXPIRED_TOKEN
}

export { AuthService, AuthServiceErrorReason };