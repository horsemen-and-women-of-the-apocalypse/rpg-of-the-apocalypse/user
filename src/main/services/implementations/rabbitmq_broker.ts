/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { IMessageBroker } from "../interfaces";
import { injectable, preDestroy } from "inversify";
import { LazyValue, Status as LazyValueStatus } from "../../utils/lazy_value";
import config from "../../config/service";
import amqp, { Channel, Connection } from "amqplib";
import { UserField } from "../../model/user";
import { Options } from "amqplib/properties";
import LOGGER from "../../logger";

const CONNECTION_URL = config.rabbitmq.url;
const EXCHANGE = "rpga.users";
const EXCHANGE_PARAMETERS: [string, string, Options.AssertExchange] = [ EXCHANGE, "direct", { durable: true } ];
const USER_REMOVAL_ROUTING_KEY = "user.removal";

/**
 * Implementation of {@link IMessageBroker} based on RabbitMQ
 */
@injectable()
class RabbitMqBroker implements IMessageBroker {
    private readonly connection: LazyValue<Promise<Connection>>;
    private readonly channel: LazyValue<Promise<Channel>>;

    /**
     * Constructor initializing the connection
     */
    constructor() {
        this.connection = new LazyValue<Promise<Connection>>(async() => {
            const conn = await amqp.connect(CONNECTION_URL);
            LOGGER.info("[RabbitMQ] Connection established");
            return conn;
        });
        this.channel = new LazyValue<Promise<Channel>>(async() => {
            const ch = await (await this.connection.get()).createChannel();

            // Define exchange
            await ch.assertExchange(...EXCHANGE_PARAMETERS);

            return ch;
        });
    }

    /**
     * Close the connection
     */
    @preDestroy()
    public async close(): Promise<void> {
        // Close connection if it was initialized
        if (this.connection.status === LazyValueStatus.INITIALIZED) {
            await (await this.connection.get()).close();
            LOGGER.info("[RabbitMQ] Connection closed");
        }
    }

    async ping(): Promise<void> {
        await this.channel.get();
    }

    async notifyUserRemoval(username: string): Promise<void> {
        (await this.channel.get()).publish(EXCHANGE, USER_REMOVAL_ROUTING_KEY, Buffer.from(JSON.stringify({ [UserField.USERNAME]: username })), {
            persistent: true,
            contentType: "application/json",
        });
    }
}

export { RabbitMqBroker, EXCHANGE, EXCHANGE_PARAMETERS, USER_REMOVAL_ROUTING_KEY };
