/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { IDatabaseService } from "../interfaces";
import { Db, MongoClient } from "mongodb";
import LOGGER from "../../logger";
import { injectable, preDestroy } from "inversify";
import { ICollection, MongoCollection } from "../database/collection";
import config from "../../config/service";
import { LazyValue, Status as LazyValueStatus } from "../../utils/lazy_value";

/**
 * Implementation of {@link IDatabaseService} based on mongodb
 */
@injectable()
class DatabaseService implements IDatabaseService {
    private client: LazyValue<Promise<MongoClient>>;

    /**
     * Constructor initializing the connection
     */
    constructor() {
        this.client = new LazyValue<Promise<MongoClient>>(() => DatabaseService.connect());
    }

    async ping(): Promise<void> {
        await (await this.database()).command({ "dbHash": 1 });
    }

    async collection(name: string): Promise<ICollection> {
        return new MongoCollection((await this.database()).collection(name));
    }

    async dropCollections(): Promise<void> {
        const drop = async(client: MongoClient) => {
            const db = client.db();

            for await (const collection of db.listCollections()) {
                await db.dropCollection(collection.name);
            }
            LOGGER.info("[Database] Collections dropped");
        };

        // Delay drop after the connection or do it immediately
        switch (this.client.status) {
            case LazyValueStatus.PENDING:
                this.client = new LazyValue<Promise<MongoClient>>(async() => {
                    const client = await DatabaseService.connect();
                    await drop(client);
                    return client;
                });
                break;

            case LazyValueStatus.INITIALIZED:
                await drop(await this.client.get());
                break;
        }
    }

    /**
     * Close the connection to the database
     */
    @preDestroy()
    public async close(): Promise<void> {
        // Close connection if it was initialized
        if (this.client.status === LazyValueStatus.INITIALIZED) {
            await (await this.client.get()).close();
            LOGGER.info("[Database] Connection closed");
        }
    }

    /**
     * Get the connected database
     */
    private async database(): Promise<Db> {
        return (await this.client.get()).db();
    }

    /**
     * Connect to the mongodb instance
     */
    private static async connect(): Promise<MongoClient> {
        const client = new MongoClient(config.database.url);
        await client.connect();
        LOGGER.info("[Database] Connection established");

        return client;
    }
}

export default DatabaseService;
