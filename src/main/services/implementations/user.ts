/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { IMessageBroker, IPasswordHashService, IUserDatabaseService, IUserService } from "../interfaces";
import { inject, injectable } from "inversify";
import { MESSAGE_BROKER, PASSWORD_HASH, USER_DATABASE } from "../instances";
import { User, UserCreationInfo, UserField } from "../../model/user";
import { UserRole } from "../../model/role";
import { Document, Filter, MongoServerError } from "mongodb";
import { ApiError } from "@rpga/common";
import passwordGenerator from "generate-password";
import { EmptyUserBuilder } from "../../model/user_builder";
import LOGGER from "../../logger";

const PASSWORD_GENERATOR_CONFIG: passwordGenerator.GenerateOptions = {
    numbers: true,
    symbols: true,
    strict: true
};

/**
 * Service exposing methods related to {@link User}s in the application
 */
@injectable()
class UserService implements IUserService {

    public static readonly OFFSET_QUERY_PARAMETER_NAME = "offset";
    public static readonly LIMIT_QUERY_PARAMETER_NAME = "limit";
    public static readonly LIMIT_QUERY_PARAMETER_MIN_VALUE = 1;
    // Why 2^31 is the max ? See https://www.mongodb.com/docs/manual/reference/method/cursor.limit/#supported-values
    public static readonly LIMIT_QUERY_PARAMETER_MAX_VALUE = 2 ** 31;
    public static readonly OFFSET_QUERY_PARAMETER_MIN_VALUE = 0;

    /** Length of random passwords */
    public static readonly PASSWORD_LENGTH = 30;

    private readonly databaseService: IUserDatabaseService;
    private readonly hash: IPasswordHashService;
    private readonly broker: IMessageBroker;

    /**
     * Constructor
     *
     * @param {IUserDatabaseService} databaseService User database service to use
     * @param {IPasswordHashService} hash Password hashing service to use
     * @param {IMessageBroker} broker Message broker to use
     */
    constructor(@inject(USER_DATABASE) databaseService: IUserDatabaseService, @inject(PASSWORD_HASH) hash: IPasswordHashService, @inject(MESSAGE_BROKER) broker: IMessageBroker) {
        this.databaseService = databaseService;
        this.hash = hash;
        this.broker = broker;
    }

    // username can be anything
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    async create(username: any): Promise<string> {
        // Generate a password
        const password = UserService.password();

        // Hash password
        const hash = await this.hash.hash(password);

        // Save user
        try {
            await this.databaseService.insert(UserCreationInfo.fromFields(username, UserRole[UserRole.USER], hash));
        } catch (e) {
            // Intercept duplicate user errors and throw a custom error
            if (e instanceof MongoServerError) {
                const eAsMongoError = e as MongoServerError;
                if (eAsMongoError.code === 11000 && eAsMongoError.message.includes(UserField.USERNAME)) {
                    throw new UserServiceError(UserServiceErrorReason.USER_ALREADY_EXISTS);
                }
            }

            throw e;
        }

        return password;
    }

    // username can be anything
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    async remove(username: any): Promise<boolean> {
        // Check username
        const validUsername = new EmptyUserBuilder().setUsername(username).build().username;

        // Remove user
        const removed = await this.databaseService.remove(validUsername);

        // Notify removal
        if (removed) {
            try {
                await this.broker.notifyUserRemoval(validUsername);
            } catch (e) {
                LOGGER.warn(`Failed to notify the removal of "${validUsername}". ${(e as Error).stack}`);
            }
        }

        return removed;
    }

    // parameters can be anything
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    list(username: any, role: any, offset: any, limit: any): Promise<User[]> {
        // Check username
        const filter: Filter<Document> = {};
        if (username !== undefined && username !== null) {
            filter[UserField.USERNAME] = {
                $regex: `^.*${new EmptyUserBuilder().setUsername(username).build().username}.*$`,
                $options: "i"
            };
        }

        // Check role
        if (role !== undefined && role !== null) {
            filter[UserField.ROLE] = UserRole[new EmptyUserBuilder().setRole(role).build().role];
        }

        // Check offset
        let off = 0;
        if (offset !== undefined && offset !== null) {
            const offsetAsNumber = Number.parseInt(offset);
            if (!Number.isInteger(offsetAsNumber) || offsetAsNumber < UserService.OFFSET_QUERY_PARAMETER_MIN_VALUE) {
                throw new UserServiceError(UserServiceErrorReason.INVALID_USER_LIST_OFFSET);
            }
            off = offsetAsNumber;
        }

        // Check limit
        let lim = 0;
        if (limit !== undefined && limit !== null) {
            const limitAsNumber = Number.parseInt(limit);
            if (!Number.isInteger(limitAsNumber) || limitAsNumber < UserService.LIMIT_QUERY_PARAMETER_MIN_VALUE || limitAsNumber > UserService.LIMIT_QUERY_PARAMETER_MAX_VALUE) {
                throw new UserServiceError(UserServiceErrorReason.INVALID_USER_LIST_LIMIT);
            }
            lim = limitAsNumber;
        }

        return this.databaseService.find(filter, off, lim);
    }

    // username can be anything
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    find(username: any): Promise<User | null> {
        return this.databaseService.findByUsername(new EmptyUserBuilder().setUsername(username).build().username);
    }

    /**
     * Generate a random password
     *
     * @return {string} Password
     */
    private static password(): string {
        return passwordGenerator.generate({
            length: UserService.PASSWORD_LENGTH,
            ...PASSWORD_GENERATOR_CONFIG
        });
    }
}

class UserServiceError extends ApiError {
    /**
     * Constructor
     *
     * @param reason Reason of the error
     */
    constructor(reason: UserServiceErrorReason) {
        super(UserServiceErrorReason[reason], 500);
    }
}

/**
 * Reasons of error in {@link UserService}
 */
enum UserServiceErrorReason {
    USER_ALREADY_EXISTS,
    INVALID_USER_LIST_OFFSET,
    INVALID_USER_LIST_LIMIT
}

export { UserService, UserServiceErrorReason };
