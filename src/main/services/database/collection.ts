/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { Collection, CreateIndexesOptions, Document, Filter, FindCursor, ObjectId, UpdateResult, WithId } from "mongodb";
import LOGGER from "../../logger";

/**
 * Interface for database collections with a read-only access
 */
interface IReadOnlyCollection {
    /**
     * Get all documents in this collection
     */
    all(): FindCursor<WithId<Document>>;

    /**
     * Find the document matching the given filter
     *
     * @param {Filter<Document>} filter Filter to use
     */
    find(filter: Filter<Document>): FindCursor<WithId<Document>>;

    /**
     * Find the first document matching the given filter
     *
     * @param {Filter<Document>} filter Filter to use
     * @return {Promise<Document | null>} Document if there is one, otherwise null
     */
    findOne(filter: Filter<Document>): Promise<Document | null>;

    /**
     * Find the document with the given identifier
     *
     * @param {ObjectId} id Identifier
     * @return {Promise<Document | null>} Document if there is one, otherwise null
     */
    findById(id: ObjectId): Promise<Document | null>;

    /**
     * Get the indices of this collection
     */
    indices(): Promise<Document[]>;
}

/**
 * Interface for mutable database collections
 */
interface ICollection extends IReadOnlyCollection {
    /**
     * Insert the given document in this collection
     *
     * @param {Document} document Document to insert
     * @return {Promise<ObjectId>} Document's identifier
     */
    insert(document: Document): Promise<ObjectId>;

    /**
     * Update the document identified by id
     *
     * @param {ObjectId} id Document id to update
     * @param {object} object Object with updated fields
     * @return {Promise<UpdateResult>} Results of the update
     */
    updateOneById(id: ObjectId, object: object): Promise<UpdateResult>;

    /**
     * Update the document matching the given filter
     *
     * @param {Filter<Document>} filter Filter to use
     * @param {object} object Object with updated fields
     * @return {Promise<UpdateResult>} Results of the update
     */
    updateOne(filter: Filter<Document>, object: object): Promise<UpdateResult>;

    /**
     * Remove the first document matching the given filter
     *
     * @param {Filter<Document>} filter Filter to use
     * @return {Promise<boolean>} true if a document was removed, false otherwise
     */
    removeOne(filter: Filter<Document>): Promise<boolean>;

    /**
     * Remove a document based on its identifier
     *
     * @param {ObjectId} id Document's identifier
     * @return {Promise<boolean>} true if a document was removed, false otherwise
     */
    removeOneById(id: ObjectId): Promise<boolean>;

    /**
     * Create indices on the given fields
     *
     * @param {string[]} fields Field names
     * @param {CreateIndexesOptions} options Index creation options
     */
    createIndices(fields: string[], options: CreateIndexesOptions): Promise<void>;
}

/**
 * Wrapper of {@link Collection} implementing {@link ICollection}
 */
class MongoCollection implements ICollection {
    private readonly collection: Collection;

    /**
     * Constructor
     *
     * @param {Collection} collection Collection to wrap
     */
    constructor(collection: Collection) {
        this.collection = collection;
    }

    all(): FindCursor<WithId<Document>> {
        return this.find({});
    }

    find(filter: Filter<Document>): FindCursor<WithId<Document>> {
        return this.collection.find(filter);
    }

    findOne(filter: Filter<Document>): Promise<Document | null> {
        return this.collection.findOne(filter);
    }

    findById(id: ObjectId): Promise<Document | null> {
        return this.findOne({ _id: id });
    }

    async insert(document: Document): Promise<ObjectId> {
        return (await this.collection.insertOne(document)).insertedId;
    }

    async updateOneById(id: ObjectId, object: object): Promise<UpdateResult> {
        return this.collection.updateOne({ _id: id }, { $set: object });
    }

    async updateOne(filter: Filter<Document>, object: object): Promise<UpdateResult> {
        const result = await this.collection.updateOne(filter, { $set: object });

        if (result.modifiedCount === 0) {
            LOGGER.warn(`[Database] Failed to update the requested document (query: ${JSON.stringify(filter)})`);
        }

        return result;
    }

    async removeOne(filter: Filter<Document>): Promise<boolean> {
        const result = await this.collection.deleteOne(filter);

        if (result.deletedCount === 0) {
            LOGGER.warn(`[Database] Failed to remove the requested document in "${this.collection.collectionName}" (query: ${JSON.stringify(filter)})`);
        }
        return result.deletedCount === 1;
    }

    removeOneById(id: ObjectId): Promise<boolean> {
        return this.removeOne({ _id: id });
    }

    async createIndices(fields: string[], options: CreateIndexesOptions): Promise<void> {
        await this.collection.createIndex(fields, options);
    }

    indices(): Promise<Document[]> {
        return this.collection.indexes();
    }
}

export { IReadOnlyCollection, ICollection, MongoCollection };
