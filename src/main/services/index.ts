/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { Container } from "inversify";
import {
    IAuthService,
    IDatabaseService,
    IReadOnlyDatabaseService,
    IMessageBroker,
    IPasswordHashService,
    IUserDatabaseService,
    IUserService,
    IVersionService
} from "./interfaces";
import {
    AUTH,
    DATABASE,
    MESSAGE_BROKER,
    PASSWORD_HASH,
    USER,
    USER_DATABASE,
    VERSION
} from "./instances";
import DatabaseService from "./implementations/database";
import UserDatabaseService from "./implementations/user_database";
import { UserService } from "./implementations/user";
import PasswordHashService from "./implementations/hash_password";
import { AuthService } from "./implementations/auth";
import VersionService from "./implementations/version";
import { RabbitMqBroker } from "./implementations/rabbitmq_broker";

/**
 * Container of services wrapping a {@link Container}
 */
class ServicesContainer {
    protected readonly container: Container;

    /**
     * Constructor
     *
     * @param {Container} container Container to use
     */
    constructor(container: Container | ServicesContainer) {
        if (container instanceof Container) {
            this.container = container;
        } else {
            this.container = container.container;
        }
    }

    /**
     * Database service
     */
    public get database(): IReadOnlyDatabaseService {
        return this.db;
    }

    /**
     * Database service (mutable version)
     */
    protected get db(): IDatabaseService {
        return this.container.get<IDatabaseService>(DATABASE);
    }

    /**
     * User service
     */
    get user(): IUserService {
        return this.container.get<IUserService>(USER);
    }

    /**
     * Get user authentication service
     */
    public get auth(): IAuthService {
        return this.container.get<IAuthService>(AUTH);
    }

    /**
     * Version service
     */
    get version(): IVersionService {
        return this.container.get<IVersionService>(VERSION);
    }

    /**
     * Message broker
     */
    public get broker(): IMessageBroker {
        return this.container.get<IMessageBroker>(MESSAGE_BROKER);
    }

    /**
     * Unbind all services in this container
     */
    public unbindAll(): Promise<void> {
        return this.container.unbindAllAsync();
    }

    /**
     * Initialize the given container with implementations to use in production
     *
     * @param {Container} container Container to initialize
     */
    static initialize(container: Container) {
        container.bind<IDatabaseService>(DATABASE).to(DatabaseService).inSingletonScope();
        container.bind<IUserDatabaseService>(USER_DATABASE).to(UserDatabaseService).inSingletonScope();
        container.bind<IUserService>(USER).to(UserService).inSingletonScope();
        container.bind<IPasswordHashService>(PASSWORD_HASH).to(PasswordHashService).inSingletonScope();
        container.bind<IAuthService>(AUTH).to(AuthService).inSingletonScope();
        container.bind<IVersionService>(VERSION).to(VersionService).inSingletonScope();
        container.bind<IMessageBroker>(MESSAGE_BROKER).to(RabbitMqBroker).inSingletonScope();
    }
}

// Register services
const container = new Container();
ServicesContainer.initialize(container);
const SERVICES = new ServicesContainer(container);

export { SERVICES, ServicesContainer };
