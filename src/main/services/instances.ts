/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */

const DATABASE = Symbol.for("IDatabaseService");
const USER_DATABASE = Symbol.for("IUserDatabaseService");
const USER = Symbol.for("IUserService");
const PASSWORD_HASH = Symbol.for("IPasswordHashService");
const VERSION = Symbol.for("IVersionService");
const AUTH = Symbol.for("IAuthService");
const MESSAGE_BROKER = Symbol.for("IMessageBroker");

export {
    DATABASE,
    USER_DATABASE,
    USER,
    PASSWORD_HASH,
    VERSION,
    AUTH,
    MESSAGE_BROKER
};
