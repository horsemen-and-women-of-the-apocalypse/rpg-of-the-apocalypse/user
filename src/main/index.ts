/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import "reflect-metadata";
import { ExpressServer } from "./server";
import LOGGER from "./logger";
import { SERVICES } from "./services";

/**
 * Script entrypoint
 */
async function main() {
    // Ping database
    try {
        await SERVICES.database.ping();
    } catch (e) {
        throw new Error(`Failed to connect to the mongo instance. ${e}`);
    }

    // Ping broker
    try {
        await SERVICES.broker.ping();
    } catch (e) {
        throw new Error(`Failed to connect to the rabbitmq instance. ${e}`);
    }

    // Create and start server
    const server = ExpressServer.create();
    await server.start();
    LOGGER.info(`[Main] Server listening on port: ${server.port} (mode: ${process.env.NODE_ENV})`);
}

main().catch(e => {
    LOGGER.error((e as Error).stack);
    process.exit(1);
});
