/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */

/**
 * Common server errors
 */
enum ServerError {
    INTERNAL_ERROR
}

export { ServerError };