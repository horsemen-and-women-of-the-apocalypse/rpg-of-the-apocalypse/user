/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import express, { Express, NextFunction, Request, Response } from "express";
import { Server } from "http";
import cors, { CorsOptions } from "cors";
import responseTime from "response-time";
import LOGGER from "./logger";
import routes from "./routes";
import swaggerUi from "swagger-ui-express";
import swaggerConfig from "./config/swagger";
import config from "./config/service";
import { ApiError, ApiErrorResponse } from "@rpga/common";
import { ServerError } from "./server_errors";
import { SERVICES } from "./services";

/**
 * {@link Server} wrapping {@link Express}
 */
class ExpressServer extends Server {

    public readonly port: number;

    private readonly express: Express;

    /**
     * Constructor
     *
     * @param {Express} express Express to wrap
     */
    private constructor(express: Express) {
        super(express);
        this.express = express;
        this.port = config.http.port;
    }

    /**
     * Start the server
     */
    start(): Promise<ExpressServer> {
        return new Promise<ExpressServer>(resolve => this.listen(this.port, () => resolve(this)));
    }

    close(callback?: (err?: Error) => void): this {
        return super.close(err => {
            SERVICES.unbindAll()
                .then(() => callback?.(err))
                .catch(e => callback?.(err === undefined ? e : new AggregateError([ err, e ])));
        });
    }

    /**
     * Create the server of the application
     */
    public static create(): ExpressServer {
        // Create server
        const app = express();
        const server = new ExpressServer(app);

        // Set up CORS
        const corsConfig: CorsOptions = { origin: true, credentials: true };
        app.use(cors(corsConfig));

        // Use JSON decoder for "application/json" body
        app.use(express.json());

        // Add response time
        app.use(responseTime((req, res, time) => {
            // req.baseUrl exits, trust me :)
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            LOGGER.info(`[Express] ${req.method} ${(req as any).baseUrl}${req.url} in ${time.toFixed(3)}ms => response code: ${res.statusCode}`);
        }));

        // Add routes
        for (const route of routes) {
            app.use(route.base, route.router);
        }

        // Add Swagger UI
        app.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerConfig));

        // Set up error handler
        app.use((error: Error, req: Request, res: Response, next: NextFunction) => {
            try {
                LOGGER.error(error);

                // Send response
                let code: number, message: string;
                if (error instanceof ApiError) {
                    const apiError = error as ApiError;
                    code = apiError.code;
                    message = error.message;
                } else {
                    code = 500;
                    message = ServerError[ServerError.INTERNAL_ERROR];
                }
                res.status(code).json(new ApiErrorResponse(message));
            } finally {
                next();
            }
        });

        return server;
    }
}

export { ExpressServer };
