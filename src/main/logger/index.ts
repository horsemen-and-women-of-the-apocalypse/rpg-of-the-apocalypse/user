/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import winston from "winston";
import WinstonDailyRotateFile from "winston-daily-rotate-file";
import { TransformableInfo } from "logform";

const TIME_FORMAT = "YYYY-MM-DD HH:mm:ss.SSS";
const PRINT_FUNC: (info: TransformableInfo) => string = info => {
    // Extract stack trace
    let message = info.message;
    if (info instanceof Error) {
        const stack = (info as Error).stack;
        if (stack !== undefined) {
            message = stack;
        }
    }

    return `${info.level}: [${info.timestamp}] ${message}`;
};

// Choose configuration depending on environment
let configuration;
switch (process.env.NODE_ENV) {
    case "production":
        configuration = {
            level: "info",
            format: winston.format.combine(
                winston.format.timestamp({ format: TIME_FORMAT }),
                winston.format.simple(),
                winston.format.printf(PRINT_FUNC)
            ),
            transports: [
                new winston.transports.Console(),
                new WinstonDailyRotateFile({
                    filename: "./log/user-%DATE%.log",
                    datePattern: "YYYY-MM-DD-HH",
                    maxSize: "20m",
                    maxFiles: "30d"
                })
            ]
        };
        break;

    case "ci":
        configuration = {
            level: "debug",
            format: winston.format.combine(
                winston.format.timestamp({ format: TIME_FORMAT }),
                winston.format.simple(),
                winston.format.printf(PRINT_FUNC)
            ),
            transports: [
                new winston.transports.Console()
            ]
        };
        break;

    default:
        configuration = {
            level: "debug",
            format: winston.format.combine(
                winston.format.colorize(),
                winston.format.timestamp({ format: TIME_FORMAT }),
                winston.format.simple(),
                winston.format.printf(PRINT_FUNC)
            ),
            transports: [
                new winston.transports.Console()
            ]
        };
}

const LOGGER = winston.createLogger(configuration);
export default LOGGER;
