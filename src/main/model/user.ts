/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { ObjectId } from "mongodb";
import { UserRole } from "./role";
import { UserModelError, UserModelErrorReason } from "./user_error";
import { EmptyUserBuilder } from "./user_builder";
import { IPasswordHashService } from "../services/interfaces";
import { AuthInfo } from "./auth_info";

/**
 * User fields
 */
enum UserField {
    ID = "_id",
    USERNAME = "username",
    ROLE = "role",
    PASSWORD = "password",
    AUTH_INFO = "authInfo"
}

/**
 * Class containing information about the user that can be shared with other users
 */
class UserView {
    public readonly username: string;
    public readonly role: UserRole;

    /**
     * Constructor
     * @param {User|UserCreationInfo} user User containing the required information
     */
    constructor(user: User | UserCreationInfo) {
        this.username = user.username;
        this.role = user.role;
    }

    toJSON() {
        return {
            [UserField.USERNAME]: this.username,
            [UserField.ROLE]: UserRole[this.role]
        };
    }
}

/**
 * Class representing a user
 */
class User {
    public readonly id: ObjectId;
    public readonly username: string;
    public readonly role: UserRole;

    private readonly password: string;
    private authInfo: AuthInfo | null;

    private readonly view: UserView;

    /**
     * Constructor initializing all fields
     *
     * @param {ObjectId} id ID
     * @param {string} username Username
     * @param {UserRole} role Role
     * @param {string} password Password
     * @param {AuthInfo} authInfo Authentication information
     */
    private constructor(id: ObjectId, username: string, role: UserRole, password: string, authInfo: AuthInfo|null) {
        this.id = id;
        this.username = username;
        this.role = role;
        this.password = password;
        this.authInfo = authInfo;

        this.view = new UserView(this);
    }

    /**
     * Compare user token with a given token
     *
     * @param {string} token Token to compare
     * @return {boolean} True if token matches with user token, false otherwise
     */
    isAuthInfoTokenEqual(token: string): boolean {
        return this.authInfo?.isTokenEqual(token) ?? false;
    }

    /**
     * Test whether this user has the given password
     *
     * @param {string} password Clear password to test
     * @param {IPasswordHashService} hash Service used to compare passwords
     * @return {Promise<boolean>} true if the given password is the user's password, false otherwise
     */
    hasPassword(password: string, hash: IPasswordHashService): Promise<boolean> {
        return hash.areEqual(password, this.password);
    }

    /**
     * Convert this object into a {@link UserView}
     */
    asView(): UserView {
        return this.view;
    }

    toJSON(): Record<string, unknown> {
        return {
            [UserField.ID]: this.id,
            [UserField.USERNAME]: this.username,
            [UserField.ROLE]: UserRole[this.role],
            [UserField.PASSWORD]: this.password,
            [UserField.AUTH_INFO]: this.authInfo
        };
    }

    /**
     * Convert the given JSON object into a {@link User}
     *
     * @param {Object} object Object to convert
     * @throws {UserModelError} If JSON contains invalid data
     */
    // object can be anything
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    public static fromJSON(object: any): User {
        // Check value
        if (object === null || object === undefined) {
            throw new UserModelError(UserModelErrorReason.UNDEFINED);
        }

        const fields = new EmptyUserBuilder()
            .setId(object[UserField.ID])
            .setUsername(object[UserField.USERNAME])
            .setRole(object[UserField.ROLE])
            .setPassword(object[UserField.PASSWORD])
            .setAuthInfo(object[UserField.AUTH_INFO], object[UserField.USERNAME])
            .build();

        return new User(fields.id, fields.username, fields.role, fields.password, fields.authInfo);
    }
}

/**
 * Class containing information required to create a user
 */
class UserCreationInfo {
    public readonly username: string;
    public readonly role: UserRole;
    public readonly password: string;

    private readonly view: UserView;

    /**
     * Constructor
     *
     * @param {string} username Username
     * @param {UserRole} role Role
     * @param {string} password Password
     */
    private constructor(username: string, role: UserRole, password: string) {
        this.username = username;
        this.role = role;
        this.password = password;

        this.view = new UserView(this);
    }

    /**
     * Convert this object into a {@link UserView}
     */
    asView(): UserView {
        return this.view;
    }

    toJSON() {
        return {
            [UserField.USERNAME]: this.username,
            [UserField.ROLE]: UserRole[this.role],
            [UserField.PASSWORD]: this.password
        };
    }

    /**
     * Convert the given JSON object into a {@link UserCreationInfo}
     *
     * @param {Object} object Object to convert
     * @throws {UserModelError} If JSON contains invalid data
     */
    // object can be anything
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    public static fromJSON(object: any): UserCreationInfo {
        // Check value
        if (object === null || object === undefined) {
            throw new UserModelError(UserModelErrorReason.UNDEFINED);
        }

        return UserCreationInfo.fromFields(object[UserField.USERNAME], object[UserField.ROLE], object[UserField.PASSWORD]);
    }

    /**
     * Create a {@link UserCreationInfo} with the given parameters
     *
     * @param username Username
     * @param role Role
     * @param password Password (hash version)
     */
    // parameters can be anything
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    public static fromFields(username: any, role: any, password: any) {
        const fields = new EmptyUserBuilder()
            .setUsername(username)
            .setRole(role)
            .setPassword(password)
            .build();

        return new UserCreationInfo(fields.username, fields.role, fields.password);
    }
}

export { User, UserField, UserCreationInfo, UserView };
