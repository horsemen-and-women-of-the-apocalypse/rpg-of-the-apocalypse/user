/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { UserModelError, UserModelErrorReason } from "./user_error";
import { tryParse, UserRole } from "./role";
import { ObjectId } from "mongodb";
import { UserField } from "./user";
import { AuthInfo } from "./auth_info";
import config from "../config/service";
import { verify } from "jsonwebtoken";

const USERNAME_REGEX = /^[a-zA-Z]{1,18}$/;

/** Type declaring an id field */
type WithId = { id: ObjectId };

/** Type declaring a username field */
type WithUsername = { [UserField.USERNAME]: string };

/** Type declaring a role field */
type WithRole = { [UserField.ROLE]: UserRole };

/** Type declaring a password field */
type WithPassword = { [UserField.PASSWORD]: string };

/** Type declaring an authentication information */
type WithAuthInfo = { authInfo: AuthInfo|null };

/**
 * Builder for user fields
 */
abstract class AUserBuilder<T> {
    private readonly fields: T;

    /**
     * Constructor
     *
     * @param fields Fields to store
     */
    protected constructor(fields: T) {
        this.fields = fields;
    }

    /**
     * Set a new identifier
     *
     * @param {any} id New identifier
     * @throws {UserModelError} If id is invalid
     */
    // id can be anything
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    public setId(id: any): AUserBuilder<T & WithId> {
        if (!ObjectId.isValid(id)) {
            throw new UserModelError(UserModelErrorReason.INVALID_ID);
        }

        return this.aggregate({ id });
    }

    /**
     * Set a new username
     *
     * @param {any} username New username
     * @throws {UserModelError} If username is invalid
     */
    // username can be anything
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    public setUsername(username: any): AUserBuilder<T & WithUsername> {
        if (typeof username !== "string" || !USERNAME_REGEX.test(username)) {
            throw new UserModelError(UserModelErrorReason.INVALID_USERNAME);
        }

        return this.aggregate({ [UserField.USERNAME]: username });
    }

    /**
     * Set a new role
     *
     * @param {any} role New role
     * @throws {UserModelError} If role is invalid
     */
    // role can be anything
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    public setRole(role: any): AUserBuilder<T & WithRole> {
        const parsedRole = tryParse(role);
        if (parsedRole === undefined) {
            throw new UserModelError(UserModelErrorReason.INVALID_ROLE);
        }

        return this.aggregate({ [UserField.ROLE]: parsedRole });
    }

    /**
     * Set a new password
     *
     * @param {any} password New password
     * @throws {UserModelError} If password is invalid
     */
    // password can be anything
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    public setPassword(password: any): AUserBuilder<T & WithPassword> {
        if (typeof password !== "string" || (password as string).length !== 60) {
            throw new UserModelError(UserModelErrorReason.INVALID_PASSWORD);
        }

        return this.aggregate({ [UserField.PASSWORD]: password });
    }

    /**
     * Set a new authentication information
     * @param {any} authInfo New authentication information
     * @param {string} username Token subject username
     * @throws {AuthInfoModelError} If auth info is invalid
     */
    // auth info can be anything
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    public setAuthInfo(authInfo: any, username: string): AUserBuilder<T & WithAuthInfo> {
        if (authInfo === undefined)
            throw new UserModelError(UserModelErrorReason.INVALID_AUTH_INFO);

        if (authInfo === null)
            return this.aggregate( { authInfo: null });

        const parsedAuthInfo = AuthInfo.fromJSON(authInfo);

        // Check if subject in token is user
        try {
            verify(
                authInfo.token,
                config.userAuth.publicKey,
                {
                    algorithms: [ config.userAuth.algorithm ],
                    subject: username,
                    ignoreExpiration: true
                });
        } catch {
            throw new UserModelError(UserModelErrorReason.INVALID_AUTH_INFO);
        }

        return this.aggregate({ authInfo: parsedAuthInfo });
    }

    /**
     * Get fields stored by this builder
     */
    public build(): T {
        return this.fields;
    }

    /**
     * Create an aggregation of this builder with the given data
     *
     * @param {E} data Data to add
     */
    private aggregate<E>(data: E): AUserBuilder<T & E> {
        const fields = this.fields;
        return new class extends AUserBuilder<T & E> {
            constructor() {
                super({ ...fields, ...data });
            }
        };
    }
}

/**
 * {@link AUserBuilder} without initial data
 */
class EmptyUserBuilder extends AUserBuilder<Record<string, never>> {
    /**
     * Constructor
     */
    constructor() {
        super({});
    }
}

export { EmptyUserBuilder, USERNAME_REGEX };
