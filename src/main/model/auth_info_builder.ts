/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { AuthInfoModelError, AuthInfoModelErrorReason } from "./auth_info_error";
import { JwtPayload, verify } from "jsonwebtoken";
import config from "../config/service";

/** Type declaring a token field */
type WithToken = { token: string };

/**
 * Builder for authentication information fields
 */
abstract class AAuthInfoBuilder<T> {
    private readonly fields: T;

    /**
     * Constructor
     *
     * @param fields Fields to store
     */
    protected constructor(fields: T) {
        this.fields = fields;
    }

    /**
     * Set a new token
     *
     * @param {any} token A JSON Web Token including issuer and subject claims
     * @throws {AuthInfoModelError} If token is invalid
     */
    // token can be anything
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    public setToken(token: any): AAuthInfoBuilder<T & WithToken> {
        if (typeof token !== "string") {
            throw new AuthInfoModelError(AuthInfoModelErrorReason.INVALID_TOKEN);
        }

        let decoded = null;
        try {
            decoded = verify(
                token,
                config.userAuth.publicKey,
                {
                    algorithms: [ config.userAuth.algorithm ],
                    issuer: config.userAuth.issuer,
                    ignoreExpiration: true
                });
        } catch (error) {
            throw new AuthInfoModelError(AuthInfoModelErrorReason.INVALID_TOKEN);
        }

        // Check for mandatory claim
        if (decoded.sub?.length === 0 || !("sub" in (decoded as JwtPayload))) {
            throw new AuthInfoModelError(AuthInfoModelErrorReason.INVALID_TOKEN);
        }

        // TODO: Check if expiration delay is consistent with configuration set value ?

        return this.aggregate({ token });
    }

    /**
     * Get fields stored by this builder
     */
    public build(): T {
        return this.fields;
    }

    /**
     * Create an aggregation of this builder with the given data
     *
     * @param {E} data Data to add
     */
    private aggregate<E>(data: E): AAuthInfoBuilder<T & E> {
        const fields = this.fields;
        return new class extends AAuthInfoBuilder<T & E> {
            constructor() {
                super({ ...fields, ...data });
            }
        };
    }
}

/**
 * {@link AAuthInfoBuilder} without initial data
 */
class EmptyAuthInfoBuilder extends AAuthInfoBuilder<Record<string, never>> {
    /**
     * Constructor
     */
    constructor() {
        super({});
    }
}

export default EmptyAuthInfoBuilder;