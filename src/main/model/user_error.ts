/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { ApiError } from "@rpga/common";

/**
 * Error related to the invalidity of the model of a user
 */
class UserModelError extends ApiError {

    /**
     * Constructor
     *
     * @param reason Reason of the error
     */
    constructor(reason: UserModelErrorReason) {
        super(UserModelErrorReason[reason], 500);
    }
}

/**
 * Reasons of error in the user model
 */
enum UserModelErrorReason {
    UNDEFINED,
    INVALID_ID,
    INVALID_USERNAME,
    INVALID_ROLE,
    INVALID_PASSWORD,
    INVALID_AUTH_INFO
}

export { UserModelError, UserModelErrorReason };
