/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { ApiError } from "@rpga/common";

/**
 * Error related to the invalidity of the model of an authentication information
 */
class AuthInfoModelError extends ApiError {
    /**
     * Constructor
     *
     * @param reason Reason of the error
     */
    constructor(reason: AuthInfoModelErrorReason) {
        super(AuthInfoModelErrorReason[reason], 500);
    }
}

/**
 * Reasons of error in {@link AuthInfo}
 */
enum AuthInfoModelErrorReason {
    UNDEFINED,
    INVALID_TOKEN,
}

export { AuthInfoModelError, AuthInfoModelErrorReason };