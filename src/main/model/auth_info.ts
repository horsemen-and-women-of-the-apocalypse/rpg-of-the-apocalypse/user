/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { AuthInfoModelErrorReason, AuthInfoModelError } from "./auth_info_error";
import EmptyAuthInfoBuilder from "./auth_info_builder";

/**
 * Auth info fields
 */
enum AuthInfoField {
    TOKEN = "token"
}

/**
 * Class representing an authentication information
 */
class AuthInfo {
    private readonly token: string;

    /**
     * Constructor initializing all fields
     *
     * @param {string} token Token
     * @private
     */
    private constructor(token: string) {
        this.token = token;
    }

    /**
     * Compares instance token with a given token
     *
     * @param {string} token Token to compare
     * @return {boolean} True if tokens match, false otherwise
     */
    isTokenEqual(token: string): boolean {
        return this.token === token;
    }

    toJSON(): Record<string, unknown> {
        return {
            [AuthInfoField.TOKEN]: this.token
        };
    }

    /**
     * Convert the given JSON object into a {@link AuthInfo}
     *
     * @param {Object} object Object to convert
     * @throws {AuthInfoModelError} If JSON contains invalid data
     */
    // object can be anything
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    public static fromJSON(object: any): AuthInfo {
        if (object === null || object === undefined) {
            throw new AuthInfoModelError(AuthInfoModelErrorReason.UNDEFINED);
        }

        const fields = new EmptyAuthInfoBuilder()
            .setToken(object[AuthInfoField.TOKEN])
            .build();

        return new AuthInfo(fields.token);
    }
}

export { AuthInfo, AuthInfoField };

