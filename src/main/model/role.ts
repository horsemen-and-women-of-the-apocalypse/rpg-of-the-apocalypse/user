/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */

/**
 * User roles
 */
enum UserRole {
    USER,
    ADMIN
}

/** All roles available */
const ROLES = [ UserRole.USER, UserRole.ADMIN ];

/**
 * Try to convert value into a {@link UserRole}
 *
 * @param value The corresponding role if value is "USER" or "ADMIN", otherwise undefined
 */
// value can be anything
// eslint-disable-next-line @typescript-eslint/no-explicit-any
function tryParse(value: any): UserRole | undefined {
    switch (value) {
        case UserRole[UserRole.USER]:
            return UserRole.USER;

        case UserRole[UserRole.ADMIN]:
            return UserRole.ADMIN;

        default:
            return undefined;
    }
}

export { UserRole, ROLES, tryParse };
