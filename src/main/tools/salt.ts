/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import bcrypt from "bcrypt";

// No need to use a LOGGER just to display a salt
// eslint-disable-next-line no-console
console.log(`Salt:
${bcrypt.genSaltSync()}`);
