/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */

/**
 * Status of a {@link LazyValue}
 */
enum Status {
    /** The value isn't initialized */
    PENDING,
    /** The value was initialized */
    INITIALIZED
}

/**
 * Container providing the lazy initialization of a value computed by a function
 */
class LazyValue<T> {
    private readonly func: () => T;

    private _status: Status = Status.PENDING;
    private value!: T;

    /**
     * Constructor
     *
     * @param {() => T} func Function computing the value
     */
    constructor(func: () => T) {
        this.func = func;
    }

    /**
     * Status
     */
    public get status(): Status {
        return this._status;
    }

    /**
     * Get the value
     *
     * @return {T} Value
     */
    public get(): T {
        if (this._status === Status.PENDING) {
            this.value = this.func();
            this._status = Status.INITIALIZED;
        }

        return this.value;
    }
}

export { LazyValue, Status };
