/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { ServerError } from "../server_errors";
import { UserService, UserServiceErrorReason } from "../services/implementations/user";
import { UserModelErrorReason } from "../model/user_error";
import { ROLES, UserRole } from "../model/role";
import { AuthServiceErrorReason } from "../services/implementations/auth";
import { AuthInfoModelErrorReason } from "../model/auth_info_error";
import { USERNAME_REGEX } from "../model/user_builder";
import config from "../config/service";

const ERRORS_DESCRIPTIONS = new Map<string, string>();
ERRORS_DESCRIPTIONS.set(ServerError[ServerError.INTERNAL_ERROR], "An internal error occurred (we cannot provide more information)");
ERRORS_DESCRIPTIONS.set(UserModelErrorReason[UserModelErrorReason.INVALID_ROLE], `Provided role is invalid, it should be either ${ROLES.map(r => UserRole[r]).join(" or ")}`);
ERRORS_DESCRIPTIONS.set(UserModelErrorReason[UserModelErrorReason.INVALID_USERNAME], `Provided username is invalid, it should respect the following regex: ${USERNAME_REGEX.source}`);
ERRORS_DESCRIPTIONS.set(UserServiceErrorReason[UserServiceErrorReason.USER_ALREADY_EXISTS], "There is already a user with the specified username");
ERRORS_DESCRIPTIONS.set(UserServiceErrorReason[UserServiceErrorReason.INVALID_USER_LIST_OFFSET], `Provided offset is invalid, it should greater than or equal to ${UserService.OFFSET_QUERY_PARAMETER_MIN_VALUE}`);
ERRORS_DESCRIPTIONS.set(UserServiceErrorReason[UserServiceErrorReason.INVALID_USER_LIST_LIMIT],
    `Provided limit is invalid, it should be inside [${UserService.LIMIT_QUERY_PARAMETER_MIN_VALUE}, ${UserService.LIMIT_QUERY_PARAMETER_MAX_VALUE}]`
);
ERRORS_DESCRIPTIONS.set(AuthServiceErrorReason[AuthServiceErrorReason.AUTH_NO_TOKEN], "No token found in authorization header");
ERRORS_DESCRIPTIONS.set(AuthServiceErrorReason[AuthServiceErrorReason.AUTH_EXPIRED_TOKEN], "Token has expired");
ERRORS_DESCRIPTIONS.set(AuthServiceErrorReason[AuthServiceErrorReason.AUTH_UNKNOWN_TOKEN], "Token is not linked to any user");
ERRORS_DESCRIPTIONS.set(AuthInfoModelErrorReason[AuthInfoModelErrorReason.INVALID_TOKEN], "Token signature is invalid or payload format is invalid");

/**
 * Create the definition of a 200 response
 *
 * @param {string} payload Link to the definition of response's data schema
 */
function createApiOkResponseDefinition(payload: string) {
    return {
        "required": [ "data", "error" ],
        "properties": {
            "data": {
                "$ref": payload
            },
            "error": {
                "enum": [ null ],
                "nullable": true
            }
        }
    };
}

/**
 * Create the definition of a KO response
 *
 * @param {string[]} errors Possible errors
 */
function createApiKoResponseDefinition(errors: string[] = []) {
    const errorEnum = errors.concat([ ServerError[ServerError.INTERNAL_ERROR] ]);
    const getDescription = (e: string): string => {
        const desc = ERRORS_DESCRIPTIONS.get(e);
        if (desc === undefined) {
            throw new Error(`The description of ${e} isn't specified`);
        }

        return desc;
    };

    return {
        "required": [ "data", "error" ],
        "properties": {
            "data": {
                "enum": [ "KO" ],
                "type": "string"
            },
            "error": {
                "enum": errorEnum,
                "type": "string",
                "description": errorEnum.map(e => `- ${e}: ${getDescription(e)}`).join("\n")
            }
        }
    };
}

/**
 * Common authorization header definition
 */
function authHeaderDefinition() {
    return {
        "in": "header",
        "name": "Authorization",
        "description": `Contains the access token in format \`${config.userAuth.prefix} {jwt}\``,
        "schema": {
            "type": "string",
        }
    };
}

export { createApiKoResponseDefinition, createApiOkResponseDefinition, authHeaderDefinition };
