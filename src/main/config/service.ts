/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import fs from "fs";
import LOGGER from "../logger";
import ini from "ini";
import { Algorithm } from "jsonwebtoken";

type IniSection = { [property: string]: string | undefined };
type Ini = { [section: string]: IniSection | undefined };

/**
 * HTTP Server configuration
 */
class HttpConfiguration {

    public static readonly SECTION_NAME = "http";

    private static readonly PORT_FIELD_NAME = "port";

    /** Port number */
    public readonly port: number;

    /**
     * Constructor
     *
     * @param {IniSection} conf Configuration as an object
     */
    constructor(conf: IniSection) {
        const port = conf[HttpConfiguration.PORT_FIELD_NAME] ?? "undefined";
        const parsedPort = Number.parseInt(port);
        if (!Number.isInteger(parsedPort)) {
            throw new Error(`${HttpConfiguration.SECTION_NAME}.${HttpConfiguration.PORT_FIELD_NAME} (value: ${port}) isn't an integer in the configuration file`);
        }
        this.port = parsedPort;
    }
}

/**
 * Database configuration
 */
class DatabaseConfiguration {

    public static readonly SECTION_NAME = "database";

    private static readonly URL_FIELD_NAME = "url";

    /** Connection URL */
    public readonly url: string;

    /**
     * Constructor
     *
     * @param {IniSection} conf Configuration as an object
     */
    constructor(conf: IniSection) {
        const url = conf[DatabaseConfiguration.URL_FIELD_NAME];
        if (typeof url !== "string") {
            throw new Error(`${DatabaseConfiguration.SECTION_NAME}.${DatabaseConfiguration.URL_FIELD_NAME} is undefined`);
        }
        this.url = url;
    }
}

/**
 * Authentication configuration
 */
class UserAuthConfiguration {

    public static readonly SECTION_NAME = "user_auth";

    private static readonly ADMIN_PASSWORD_FIELD_NAME = "admin_password";
    private static readonly SALT_FIELD_NAME = "salt";
    private static readonly ISSUER_FIELD_NAME = "issuer";
    private static readonly HEADER_PREFIX = "prefix";
    private static readonly EXPIRES_IN_FIELD_NAME = "expires_in";
    private static readonly PRIVATE_KEY_FILE_FIELD_NAME = "private_key_file";
    private static readonly PUBLIC_KEY_FILE_FIELD_NAME = "public_key_file";

    /** Password salt */
    public readonly salt: string;
    /** Administrator password */
    public readonly adminPassword: string;
    /** Issuer name */
    public readonly issuer: string;
    /** Header prefix */
    public readonly prefix: string;
    /** Expiration delay */
    public readonly expiresIn: string;
    /** Algorithm used for JWTs */
    public readonly algorithm: Algorithm = "RS256";
    /** Private key */
    public readonly privateKey: string;
    /** Public key */
    public readonly publicKey: string;

    /**
     * Constructor
     *
     * @param {IniSection} conf Configuration as an object
     */
    constructor(conf: IniSection) {
        const issuer = conf[UserAuthConfiguration.ISSUER_FIELD_NAME];
        const prefix = conf[UserAuthConfiguration.HEADER_PREFIX];
        const privateKeyFile = conf[UserAuthConfiguration.PRIVATE_KEY_FILE_FIELD_NAME];
        const publicKeyFile = conf[UserAuthConfiguration.PUBLIC_KEY_FILE_FIELD_NAME];
        const expiresIn = conf[UserAuthConfiguration.EXPIRES_IN_FIELD_NAME];

        const salt = conf[UserAuthConfiguration.SALT_FIELD_NAME];
        if (typeof salt !== "string") {
            throw new Error(`${UserAuthConfiguration.SECTION_NAME}.${UserAuthConfiguration.SALT_FIELD_NAME} is undefined`);
        }
        this.salt = salt;

        const adminPassword = conf[UserAuthConfiguration.ADMIN_PASSWORD_FIELD_NAME];
        if (typeof adminPassword !== "string") {
            throw new Error(`${UserAuthConfiguration.SECTION_NAME}.${UserAuthConfiguration.ADMIN_PASSWORD_FIELD_NAME} is undefined`);
        }
        this.adminPassword = adminPassword;

        if (typeof issuer !== "string") {
            throw new Error(`${UserAuthConfiguration.SECTION_NAME}.${UserAuthConfiguration.ISSUER_FIELD_NAME} is undefined`);
        }

        if (typeof prefix !== "string") {
            throw new Error(`${UserAuthConfiguration.SECTION_NAME}.${UserAuthConfiguration.HEADER_PREFIX} is undefined`);
        }

        if (/\s/g.test(prefix)) {
            throw new Error(`${UserAuthConfiguration.SECTION_NAME}.${UserAuthConfiguration.HEADER_PREFIX} must not have white spaces.`);
        }

        if (typeof expiresIn !== "string") {
            throw new Error(`${UserAuthConfiguration.SECTION_NAME}.${UserAuthConfiguration.EXPIRES_IN_FIELD_NAME} is undefined`);
        }

        if (typeof privateKeyFile !== "string") {
            throw new Error(`${UserAuthConfiguration.SECTION_NAME}.${UserAuthConfiguration.PRIVATE_KEY_FILE_FIELD_NAME} is undefined`);
        }

        if (typeof publicKeyFile !== "string") {
            throw new Error(`${UserAuthConfiguration.SECTION_NAME}.${UserAuthConfiguration.PUBLIC_KEY_FILE_FIELD_NAME} is undefined`);
        }

        const privateKey = fs.readFileSync(privateKeyFile, "utf-8");
        const publicKey = fs.readFileSync(publicKeyFile, "utf-8");

        this.issuer = issuer;
        this.prefix = prefix;
        this.expiresIn = expiresIn;
        this.privateKey = privateKey;
        this.publicKey = publicKey;
    }
}

/**
 * RabbitMQ configuration
 */
class RabbitMqConfiguration {

    public static readonly SECTION_NAME = "rabbitmq";

    private static readonly URL_FIELD_NAME = "url";

    /** Connection URL */
    public readonly url: string;

    /**
     * Constructor
     *
     * @param {IniSection} conf Configuration as an object
     */
    constructor(conf: IniSection) {
        const url = conf[RabbitMqConfiguration.URL_FIELD_NAME];
        if (typeof url !== "string") {
            throw new Error(`${RabbitMqConfiguration.SECTION_NAME}.${RabbitMqConfiguration.URL_FIELD_NAME} is undefined`);
        }
        this.url = url;
    }
}

/**
 * Service configuration
 */
class Configuration {

    public readonly database: DatabaseConfiguration;
    public readonly http: HttpConfiguration;
    public readonly userAuth: UserAuthConfiguration;
    public readonly rabbitmq: RabbitMqConfiguration;

    /**
     * Constructor
     *
     * @param conf Configuration as an object
     */
    constructor(conf: Ini) {
        const http = conf[HttpConfiguration.SECTION_NAME];
        if (http === undefined || http === null) {
            throw new Error(`${HttpConfiguration.SECTION_NAME} section is undefined in the configuration file`);
        }
        this.http = new HttpConfiguration(http);

        const database = conf[DatabaseConfiguration.SECTION_NAME];
        if (database === undefined || database === null) {
            throw new Error(`${DatabaseConfiguration.SECTION_NAME} section is undefined in the configuration file`);
        }
        this.database = new DatabaseConfiguration(database);

        const userAuth = conf[UserAuthConfiguration.SECTION_NAME];
        if (userAuth === undefined || userAuth === null) {
            throw new Error(`${UserAuthConfiguration.SECTION_NAME} section is undefined in the configuration file`);
        }
        this.userAuth = new UserAuthConfiguration(userAuth);

        const rabbitmq = conf[RabbitMqConfiguration.SECTION_NAME];
        if (rabbitmq === undefined || rabbitmq === null) {
            throw new Error(`${RabbitMqConfiguration.SECTION_NAME} section is undefined in the configuration file`);
        }
        this.rabbitmq = new RabbitMqConfiguration(rabbitmq);
    }
}

// Load configuration
const configPath = "./config/app." + process.env.NODE_ENV + ".ini";
LOGGER.info("Try to load configuration from: " + configPath);
const config = new Configuration(ini.parse(fs.readFileSync(configPath, { encoding: "utf-8" })));
LOGGER.info("Configuration loaded");

export default config;
