/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { UserService, UserServiceErrorReason } from "../services/implementations/user";
import { authHeaderDefinition, createApiKoResponseDefinition, createApiOkResponseDefinition } from "../utils/swagger";
import { AuthInfoModelErrorReason } from "../model/auth_info_error";
import { AuthServiceErrorReason } from "../services/implementations/auth";
import { UserModelErrorReason } from "../model/user_error";
import { USERNAME_REGEX } from "../model/user_builder";
import VersionService from "../services/implementations/version";
import { UserField } from "../model/user";
import { ROLES, UserRole } from "../model/role";

const USER_USERNAME_SCHEMA = {
    "type": "string",
    "pattern": USERNAME_REGEX.source,
    "description": "Identifier of the user"
};
const USER_ROLE_SCHEMA = {
    "type": "string",
    "enum": ROLES.map(r => UserRole[r]),
    "description": "Role of the user"
};
const USER_VIEW_SCHEMA = {
    "type": "object",
    "required": [ UserField.USERNAME, UserField.ROLE ],
    "properties": {
        [UserField.USERNAME]: USER_USERNAME_SCHEMA,
        [UserField.ROLE]: {
            "type": "string",
            "enum": ROLES.map(r => UserRole[r]),
            "description": "Role of the user"
        }
    }
};

const swaggerConfig = {
    "swagger": "2.0",
    "info": {
        "version": process.env.npm_package_version,
        "title": "RPG of the Apocalypse - User service",
        "description": "Documentation describing the endpoints of the user service",
        "license": {
            "name": "MIT",
            "url": "https://opensource.org/licenses/MIT"
        }
    },
    "tags": [
        {
            "name": "Version",
            "description": "API for version"
        },
        {
            "name": "Users",
            "description": "API for users"
        },
        {
            "name": "Authentication",
            "description": "API for user authentication"
        }
    ],
    "basePath": "/",
    "schemes": [
        "http"
    ],
    "consumes": [
        "application/json"
    ],
    "produces": [
        "application/json"
    ],
    "paths": {
        "/version": {
            "get": {
                "tags": [ "Version" ],
                "summary": "Get the version of the API",
                "responses": {
                    "200": {
                        "description": "The version of the API",
                        "schema": {
                            "$ref": "#/definitions/VersionResponse"
                        }
                    },
                    "500": {
                        "description": "An error occurred",
                        "schema": {
                            "$ref": "#/definitions/VersionErrorResponse"
                        }
                    }
                }
            }
        },
        "/users": {
            "post": {
                "tags": [ "Users" ],
                "summary": "Create a user with the specified username",
                // TODO: Add requirements (auth + admin role)
                "parameters": [
                    {
                        "in": "body",
                        "name": "UserCreateInfo",
                        "required": true,
                        "description": "The information required to create a user",
                        "schema": {
                            "$ref": "#/definitions/UserCreateInfo"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "The user is created",
                        "schema": {
                            "$ref": "#/definitions/CreateUserResponse"
                        }
                    },
                    // TODO: Add auth/role errors
                    "500": {
                        "description": "Failed to create the user",
                        "schema": {
                            "$ref": "#/definitions/CreateUserInternalErrorResponse"
                        }
                    }
                }
            },
            "get": {
                "tags": [ "Users" ],
                "summary": "List users",
                // TODO: Add requirements (auth + admin role)
                "parameters": [
                    {
                        "in": "query",
                        "name": UserField.USERNAME,
                        "description": "The text that the username of returned users must contain",
                        "required": false,
                        "schema": USER_USERNAME_SCHEMA
                    },
                    {
                        "in": "query",
                        "name": UserField.ROLE,
                        "description": "The role that the returned users must have",
                        "required": false,
                        "schema": USER_ROLE_SCHEMA
                    },
                    {
                        "in": "query",
                        "name": UserService.OFFSET_QUERY_PARAMETER_NAME,
                        "description": "The number of users to skip in the list of returned users",
                        "required": false,
                        "schema": {
                            "type": "integer",
                            "minimum": 0
                        }
                    },
                    {
                        "in": "query",
                        "name": UserService.LIMIT_QUERY_PARAMETER_NAME,
                        "description": "The maximum number of users to return",
                        "required": false,
                        "schema": {
                            "type": "integer",
                            "minimum": 1,
                            "maximum": UserService.LIMIT_QUERY_PARAMETER_MAX_VALUE
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "List of users matching the query",
                        "schema": {
                            "$ref": "#/definitions/ListUsersResponse"
                        }
                    },
                    // TODO: Add auth/role errors
                    "500": {
                        "description": "Failed to retrieve users",
                        "schema": {
                            "$ref": "#/definitions/ListUsersInternalErrorResponse"
                        }
                    }
                }
            }
        },
        "/users/{username}": {
            "delete": {
                "tags": [ "Users" ],
                "summary": "Remove a user with the specified username",
                // TODO: Add requirements (auth + admin role)
                "parameters": [
                    {
                        "in": "path",
                        "name": "username",
                        "required": true,
                        "description": "The username of the user to delete",
                        "schema": USER_USERNAME_SCHEMA
                    }
                ],
                "responses": {
                    "200": {
                        "description": "The user was removed or not",
                        "schema": {
                            "$ref": "#/definitions/DeleteUserResponse"
                        }
                    },
                    // TODO: Add auth/role errors
                    "500": {
                        "description": "Failed to create the user",
                        "schema": {
                            "$ref": "#/definitions/DeleteUserInternalErrorResponse"
                        }
                    }
                }
            },
            "get": {
                "tags": [ "Users" ],
                "summary": "Fetch the information of the specified user",
                // TODO: Add requirements (auth + same user)
                "parameters": [
                    {
                        "in": "path",
                        "name": "username",
                        "required": true,
                        "description": "The username of the user",
                        "schema": USER_USERNAME_SCHEMA
                    }
                ],
                "responses": {
                    "200": {
                        "description": "The information of the user",
                        "schema": {
                            "$ref": "#/definitions/FetchUserResponse"
                        }
                    },
                    // TODO: Add auth/same user errors
                    "500": {
                        "description": "Failed to fetch user's information",
                        "schema": {
                            "$ref": "#/definitions/FetchUserInternalErrorResponse"
                        }
                    }
                }
            }
        },
        "/auth/verify": {
            "get": {
                "tags": [ "Authentication" ],
                "summary": "Check a token (in format 'RPGA-Auth {JWT}') in request authentication header, if it is linked to some user and if it has not expired",
                "parameters": [
                    authHeaderDefinition()
                ],
                "responses": {
                    "200": {
                        "description": "The token is valid",
                        "schema": {
                            "$ref": "#/definitions/VerifyTokenResponse"
                        }
                    },
                    "500": {
                        "description": "The token is invalid",
                        "schema": {
                            "$ref": "#/definitions/VerifyTokenInternalErrorResponse"
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "VersionResponse": createApiOkResponseDefinition("#/definitions/VersionResponseData"),
        "VersionResponseData": {
            "type": "string",
            "description": VersionService.VERSION_FORMAT_DESCRIPTION
        },
        "VersionErrorResponse": createApiKoResponseDefinition(),
        "VerifyTokenResponse": createApiOkResponseDefinition("#/definitions/VerifyTokenResponseData"),
        "VerifyTokenInternalErrorResponse": createApiKoResponseDefinition([
            AuthServiceErrorReason[AuthServiceErrorReason.AUTH_EXPIRED_TOKEN],
            AuthServiceErrorReason[AuthServiceErrorReason.AUTH_NO_TOKEN],
            AuthServiceErrorReason[AuthServiceErrorReason.AUTH_UNKNOWN_TOKEN],
            AuthInfoModelErrorReason[AuthInfoModelErrorReason.INVALID_TOKEN],
        ]),
        "VerifyTokenResponseData": {
            "type": "string",
            "description": "'OK' if the token is valid"
        },
        "UserCreateInfo": {
            "type": "object",
            "required": [ UserField.USERNAME ],
            "properties": {
                [UserField.USERNAME]: USER_USERNAME_SCHEMA
            }
        },
        "CreateUserResponse": createApiOkResponseDefinition("#/definitions/CreateUserResponseData"),
        "CreateUserResponseData": {
            "type": "string",
            "description": `Password of the user (characters: ${UserService.PASSWORD_LENGTH})`
        },
        "CreateUserInternalErrorResponse": createApiKoResponseDefinition([
            UserServiceErrorReason[UserServiceErrorReason.USER_ALREADY_EXISTS],
            UserModelErrorReason[UserModelErrorReason.INVALID_USERNAME]
        ]),
        "ListUsersResponse": createApiOkResponseDefinition("#/definitions/ListUsersResponseData"),
        "ListUsersResponseData": {
            "type": "array",
            "items": USER_VIEW_SCHEMA,
            "description": "List of users"
        },
        "ListUsersInternalErrorResponse": createApiKoResponseDefinition([
            UserModelErrorReason[UserModelErrorReason.INVALID_USERNAME],
            UserModelErrorReason[UserModelErrorReason.INVALID_ROLE],
            UserServiceErrorReason[UserServiceErrorReason.INVALID_USER_LIST_OFFSET],
            UserServiceErrorReason[UserServiceErrorReason.INVALID_USER_LIST_LIMIT]
        ]),
        "DeleteUserResponse": createApiOkResponseDefinition("#/definitions/DeleteUserResponseData"),
        "DeleteUserResponseData": {
            "type": "boolean",
            "description": "true if the user was removed, false otherwise"
        },
        "DeleteUserInternalErrorResponse": createApiKoResponseDefinition([
            UserModelErrorReason[UserModelErrorReason.INVALID_USERNAME]
        ]),
        "FetchUserResponse": createApiOkResponseDefinition("#/definitions/FetchUserResponseData"),
        "FetchUserResponseData": {
            "description": "The user's information or null if it doesn't exist",
            "nullable": true,
            ...USER_VIEW_SCHEMA
        },
        "FetchUserInternalErrorResponse": createApiKoResponseDefinition([
            UserModelErrorReason[UserModelErrorReason.INVALID_USERNAME],
        ])
    }
};

export default swaggerConfig;
