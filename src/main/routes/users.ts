/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { Router } from "express";
import { ApiResponse } from "@rpga/common";
import { SERVICES } from "../services";
import { UserField } from "../model/user";
import { UserService } from "../services/implementations/user";

const router: Router = Router();

// Creation route
router.post("/", async(req, res, next) => {
    try {
        // TODO: Check if user is connected and is an administrator

        // Create user
        const userService = SERVICES.user;
        const password = await userService.create((req?.body ?? {})[UserField.USERNAME]);

        // Send password
        res.send(new ApiResponse(password));
    } catch (e) {
        next(e);
    }
});

// Delete route
router.delete("/:username", async(req, res, next) => {
    try {
        // TODO: Check if user is connected and is an administrator

        // Remove user
        const removed = await SERVICES.user.remove(req.params.username);

        // Send status
        res.send(new ApiResponse(removed));
    } catch (e) {
        next(e);
    }
});

// List route
router.get("/", async(req, res, next) => {
    try {
        // TODO: Check if user is connected and is an administrator

        // List users
        const users = await SERVICES.user.list(
            req.query[UserField.USERNAME],
            req.query[UserField.ROLE],
            req.query[UserService.OFFSET_QUERY_PARAMETER_NAME],
            req.query[UserService.LIMIT_QUERY_PARAMETER_NAME]
        );

        // Return some information of the users
        res.send(new ApiResponse(users.map(u => u.asView())));
    } catch (e) {
        next(e);
    }
});

// Fetch route
router.get("/:username", async(req, res, next) => {
    try {
        // TODO: Check if user is connected and has the given username

        // Find user
        const user = await SERVICES.user.find(req.params.username);

        // Send user
        res.send(new ApiResponse(user?.asView() ?? null));
    } catch (e) {
        next(e);
    }
});

export default router;
