/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { Router } from "express";
import { ApiResponse } from "@rpga/common";
import { SERVICES } from "../services";

const router: Router = Router();

// Access token verification route
router.get("/verify", async(req, res, next) => {
    try {
        const authHeader = req.headers.authorization;

        await SERVICES.auth.check(authHeader?.split(" ")[1]);

        res.send(new ApiResponse("OK"));
    } catch (e) {
        next(e);
    }
});

export default router;