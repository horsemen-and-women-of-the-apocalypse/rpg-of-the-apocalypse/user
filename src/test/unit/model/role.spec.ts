/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { describe, it } from "mocha";
import { expect } from "chai";
import { ROLES } from "../../../main/model/role";

describe("Role model", () => {
    it("There are multiple roles", () => {
        expect(ROLES).to.have.lengthOf(2, "ROLES contains 2 roles");
    });
});
