/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { it } from "mocha";
import { expect } from "chai";
import { sign } from "jsonwebtoken";
import { AuthInfo } from "../../../main/model/auth_info";
import { AuthInfoModelError, AuthInfoModelErrorReason } from "../../../main/model/auth_info_error";
import config from "../../../main/config/service";
import { generateKeyPairSync } from "crypto";

const USERNAME = "xXTheKillerXx";
const PRIVATE_KEY = config.userAuth.privateKey;
const TOKEN = sign(
    {},
    PRIVATE_KEY,
    {
        algorithm: config.userAuth.algorithm,
        issuer: config.userAuth.issuer,
        subject: USERNAME,
        expiresIn: config.userAuth.expiresIn
    });

describe("AuthInfo model", () => {
    it(`A valid ${AuthInfo.name} can be parsed from and exported to JSON`, () => {
        const json = {
            token: TOKEN
        };

        const authInfo = AuthInfo.fromJSON(json);
        expect(authInfo).to.be.instanceof(AuthInfo, `${AuthInfo.fromJSON} should return a ${AuthInfo.name}`);
        expect(authInfo.toJSON()).to.deep.eq(json,"Data is unchanged");
    });

    describe(`${AuthInfo.name}#${AuthInfo.fromJSON.name}`, () => {
        it("An authentication information with an invalid token should throw an error during parsing", () => {
            const { privateKey } = generateKeyPairSync("rsa", {
                modulusLength: 1024,
                publicKeyEncoding: {
                    type: "spki",
                    format: "pem"
                },
                privateKeyEncoding: {
                    type: "pkcs8",
                    format: "pem"
                }
            });

            const WRONGLY_SIGNED_TOKEN = sign(
                {},
                privateKey,
                {
                    algorithm: config.userAuth.algorithm,
                    issuer: config.userAuth.issuer,
                    subject: USERNAME,
                    expiresIn: config.userAuth.expiresIn
                });

            const WRONG_ISSUER_TOKEN = sign(
                {},
                PRIVATE_KEY,
                {
                    algorithm: config.userAuth.algorithm,
                    issuer: "RPG of the Miracle",
                    subject: USERNAME,
                    expiresIn: config.userAuth.expiresIn
                });

            const MISSING_SUBJECT_TOKEN = sign(
                {},
                PRIVATE_KEY,
                {
                    algorithm: config.userAuth.algorithm,
                    issuer: config.userAuth.issuer,
                    expiresIn: config.userAuth.expiresIn
                });

            const EMPTY_SUBJECT_TOKEN = sign(
                {},
                PRIVATE_KEY,
                {
                    algorithm: config.userAuth.algorithm,
                    issuer: config.userAuth.issuer,
                    subject: "",
                    expiresIn: config.userAuth.expiresIn
                });

            for (const token of [ "\\o//", undefined, null, 0, "", WRONGLY_SIGNED_TOKEN, WRONG_ISSUER_TOKEN, MISSING_SUBJECT_TOKEN, EMPTY_SUBJECT_TOKEN ]) {
                expect(() => AuthInfo.fromJSON({ token: token }))
                    .to.throw(AuthInfoModelError, AuthInfoModelErrorReason[AuthInfoModelErrorReason.INVALID_TOKEN]);
            }
        });
    });

    it("undefined and null are invalid inputs", () => {
        for (const authInfo of [ undefined, null ]) {
            expect(() => AuthInfo.fromJSON(authInfo))
                .to.throw(AuthInfoModelError, AuthInfoModelErrorReason[AuthInfoModelErrorReason.UNDEFINED], `${authInfo} is an invalid authentication information`);
        }
    });
});