/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { describe, it } from "mocha";
import { expect } from "chai";
import { ObjectId } from "mongodb";
import { User, UserCreationInfo, UserField } from "../../../main/model/user";
import { ROLES, UserRole } from "../../../main/model/role";
import { UserModelError, UserModelErrorReason } from "../../../main/model/user_error";
import PasswordHashService from "../../../main/services/implementations/hash_password";
import { sign } from "jsonwebtoken";
import { AuthInfo } from "../../../main/model/auth_info";
import config from "../../../main/config/service";

// Valid fields
const ID = new ObjectId();
const USERNAME = "pushingtoothelimit";
const ROLE = UserRole[UserRole.USER];
const PASSWORD = new PasswordHashService().hashSync("password");
const TOKEN = sign(
    {},
    config.userAuth.privateKey,
    {
        algorithm: config.userAuth.algorithm,
        issuer: config.userAuth.issuer,
        subject: USERNAME,
        expiresIn: config.userAuth.expiresIn
    });
const AUTH_INFO = AuthInfo.fromJSON({ token: TOKEN });

describe("User model", () => {
    for (const { _class, refSupplier, parser, exporter } of [
        {
            _class: User,
            refSupplier: (r: UserRole) => ({ [UserField.ID]: ID, [UserField.USERNAME]: USERNAME, [UserField.ROLE]: UserRole[r], [UserField.PASSWORD]: PASSWORD, [UserField.AUTH_INFO]: AUTH_INFO }),
            parser: User.fromJSON,
            exporter: User.prototype.toJSON
        },
        {
            _class: User,
            refSupplier: (r: UserRole) => ({ [UserField.ID]: ID, [UserField.USERNAME]: USERNAME, [UserField.ROLE]: UserRole[r], [UserField.PASSWORD]: PASSWORD, [UserField.AUTH_INFO]: null }),
            parser: User.fromJSON,
            exporter: User.prototype.toJSON
        },
        {
            _class: UserCreationInfo,
            refSupplier: (r: UserRole) => ({ [UserField.USERNAME]: USERNAME, [UserField.ROLE]: UserRole[r], [UserField.PASSWORD]: PASSWORD }),
            parser: UserCreationInfo.fromJSON,
            exporter: UserCreationInfo.prototype.toJSON
        }
    ]) {
        it(`A valid ${_class.name} can be parsed from and exported to JSON`, () => {
            for (const role of ROLES) {
                const reference = refSupplier(role);

                const data = parser(reference);
                expect(data).to.be.instanceof(_class, `${parser.name} should return a ${_class.name}`);
                expect(exporter.bind(data)()).to.deep.eq(reference, "Data is unchanged");
            }
        });
    }

    describe(`${User.name}#${User.fromJSON.name}`, () => {
        it("A user with an expired token can be parsed from and exported to JSON", async() => {
            const EXPIRED_TOKEN = sign(
                {},
                config.userAuth.privateKey,
                {
                    algorithm: config.userAuth.algorithm,
                    issuer: config.userAuth.issuer,
                    subject: USERNAME,
                    expiresIn: "1s"
                });
            await new Promise(resolve => setTimeout(resolve, 1100));

            const reference = {
                _id: ID,
                username: USERNAME,
                role: ROLE,
                password: PASSWORD,
                authInfo: {
                    token: EXPIRED_TOKEN
                }
            };

            const data = User.fromJSON(reference);
            expect(data).to.be.instanceof(User, `${User.fromJSON.name} should return a ${User.name}`);
            expect(data.toJSON()).to.deep.eq(reference, "Data is unchanged");
        });

        it("A user with an invalid id should throw an error during parsing", () => {
            for (const id of [ "not-an-id", undefined, null ]) {
                expect(() => User.fromJSON({ _id: id }))
                    .to.throw(UserModelError, UserModelErrorReason[UserModelErrorReason.INVALID_ID], `${id} isn't a ObjectId`);
            }
        });
    });

    for (const { _class, parser } of [
        { _class: User, parser: User.fromJSON },
        { _class: UserCreationInfo, parser: UserCreationInfo.fromJSON }
    ]) {
        describe(`${_class.name}#${parser.name}`, () => {
            it("undefined and null are invalid inputs", () => {
                for (const user of [ undefined, null ]) {
                    expect(() => parser(user))
                        .to.throw(UserModelError, UserModelErrorReason[UserModelErrorReason.UNDEFINED], `${user} is an invalid user`);
                }
            });

            it("A user with an invalid username should throw an error during parsing", () => {
                for (const username of [ "", "haaaaaaaaaaaaaaaaaa", "sot353", "not/a/path", undefined, null ]) {
                    expect(() => parser({ _id: ID, username }))
                        .to.throw(UserModelError, UserModelErrorReason[UserModelErrorReason.INVALID_USERNAME], `${username} isn't a valid username`);
                }
            });

            it("A user with an invalid role should throw an error during parsing", () => {
                for (const role of [ "MANAGER", UserRole.USER, undefined, null ]) {
                    expect(() => parser({ _id: ID, [UserField.USERNAME]: USERNAME, role }))
                        .to.throw(UserModelError, UserModelErrorReason[UserModelErrorReason.INVALID_ROLE], `${role} isn't a valid role`);
                }
            });

            it("A user with an invalid password should throw an error during parsing", () => {
                for (const password of [ "", undefined, null ]) {
                    expect(() => parser({ _id: ID, [UserField.USERNAME]: USERNAME, [UserField.ROLE]: ROLE, password }))
                        .to.throw(UserModelError, UserModelErrorReason[UserModelErrorReason.INVALID_PASSWORD], `${password} isn't a valid password`);
                }
            });
        });
    }

    describe(`${User.name}#${User.fromJSON.name}`, () => {
        it("A user with an undefined authentication information should throw an error during parsing", () => {
            expect(() => User.fromJSON({ _id: ID, username: USERNAME, role: ROLE, password: PASSWORD }))
                .to.throw(UserModelError, UserModelErrorReason[UserModelErrorReason.INVALID_AUTH_INFO]);
        });

        it("A token with a subject who does not match with user should throw an error during parsing", () => {
            const wrongToken = sign(
                {},
                config.userAuth.privateKey,
                {
                    algorithm: config.userAuth.algorithm,
                    issuer: config.userAuth.issuer,
                    subject: "hackerman",
                    expiresIn: config.userAuth.expiresIn
                });
            expect(() => User.fromJSON({ _id: ID, username: USERNAME, role: ROLE, password: PASSWORD, authInfo: { token: wrongToken } }))
                .to.throw(UserModelError, UserModelErrorReason[UserModelErrorReason.INVALID_AUTH_INFO]);
        });
    });
});
