/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { describe, it } from "mocha";
import { LazyValue, Status } from "../../../main/utils/lazy_value";
import { expect } from "chai";

describe("LazyValue", () => {
    describe("LazyValue#get", () => {
        it("get should always return the same object", () => {
            const obj = {};
            let callCount = 0;
            const lazy = new LazyValue(() => {
                callCount++;
                return obj;
            });
            expect(lazy.status).to.eq(Status.PENDING);
            expect(callCount).to.eq(0);

            const value = lazy.get();
            expect(lazy.status).to.eq(Status.INITIALIZED);
            expect(value).to.eq(obj);
            expect(callCount).to.eq(1);

            expect(lazy.get()).to.eq(obj);
            expect(lazy.status).to.eq(Status.INITIALIZED);
            expect(callCount).to.eq(1);
        });
    });
});
