/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { SERVICES, ServicesContainer } from "../../main/services";
import {
    IDatabaseService,
    IMessageBroker,
    IPasswordHashService,
    IUserDatabaseService,
    IVersionService,
} from "../../main/services/interfaces";
import { MESSAGE_BROKER, PASSWORD_HASH, USER_DATABASE, VERSION } from "../../main/services/instances";

/**
 * {@link ServicesContainer} with methods for testing purpose
 */
class TestingServicesContainer extends ServicesContainer {
    /**
     * Shallow copy constructor
     *
     * @param {ServicesContainer} container Container to copy
     */
    constructor(container: ServicesContainer) {
        super(container);
    }

    /**
     * Get database service
     */
    get database(): IDatabaseService {
        return this.db;
    }

    /**
     * User database service
     */
    public get userDatabase(): IUserDatabaseService {
        return this.container.get<IUserDatabaseService>(USER_DATABASE);
    }

    /**
     * Password hashing service
     */
    public get hash(): IPasswordHashService {
        return this.container.get<IPasswordHashService>(PASSWORD_HASH);
    }

    /**
     * Replace the implementation of {@link IVersionService} to use
     *
     * @param {IVersionService} service New service to use
     */
    public async replaceVersion(service: IVersionService) {
        await this.container.unbindAsync(VERSION);
        this.container.bind<IVersionService>(VERSION).toConstantValue(service);
    }

    /**
     * Replace the implementation of {@link IMessageBroker} to use
     *
     * @param {IMessageBroker} service New service to use
     */
    public async replaceMessageBroker(service: IMessageBroker) {
        await this.container.unbindAsync(MESSAGE_BROKER);
        this.container.bind<IMessageBroker>(MESSAGE_BROKER).toConstantValue(service);
    }

    /**
     * Reset the services
     */
    public async reset(): Promise<void> {
        await this.unbindAll();
        ServicesContainer.initialize(this.container);
    }
}

export default new TestingServicesContainer(SERVICES);
