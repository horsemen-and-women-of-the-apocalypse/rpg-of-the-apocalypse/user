/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { describe, it } from "mocha";
import axios, { AxiosRequestConfig } from "axios";
import { ApiResponse } from "@rpga/common";
import { expect } from "chai";
import config from "../../../../main/config/service";
import { UserField, UserView } from "../../../../main/model/user";
import UserDatabaseService from "../../../../main/services/implementations/user_database";
import { UserRole } from "../../../../main/model/role";

type UserFetchResponseType = UserView | undefined;

const AXIOS_CONFIG: AxiosRequestConfig = { validateStatus: () => true };

function url(username: string) {
    return `http://localhost:${config.http.port}/users/${username}`;
}

describe("API tests", () => {
    describe("/users/:username [GET]", function() {
        /* TODO: Uncomment when it's implemented
        it("/users/:username should return an error if the request is done by a user that isn't authenticated", async() => {
            const response = await axios.get<ApiResponse<string>>(url("test"), AXIOS_CONFIG);
            // TODO: Assert http code

            // Assert response
            const responseData = response.data;
            expect(responseData).to.be.not.null.and.to.be.not.undefined;
            expect(responseData.error).to.be.deep.eq("AUTH_NO_TOKEN"); // TODO: Replace by enum value
            expect(responseData.data).to.be.deep.eq("KO");
        });

        it("/users/:username should return an error if a user fetches information of another user", async() => {
            const response = await axios.get<ApiResponse<string>>(url("test"), AXIOS_CONFIG);
            // TODO: Assert http code

            // Assert response
            const responseData = response.data;
            expect(responseData).to.be.not.null.and.to.be.not.undefined;
            expect(responseData.error).to.be.deep.eq("USER_UNAUTHORIZED"); // TODO: Replace by enum value
            expect(responseData.data).to.be.deep.eq("KO");
        });
        */

        it("/users/:username should return user's information if it exists", async() => {
            const response = await axios.get<ApiResponse<UserFetchResponseType>>(url("admin"), AXIOS_CONFIG);
            expect(response.status).to.eq(200);

            // Assert response
            const responseData = response.data;
            expect(responseData).to.be.not.null.and.to.be.not.undefined;
            expect(responseData.error).to.be.null;
            expect(responseData.data).to.be.deep.eq({
                [UserField.USERNAME]: UserDatabaseService.ADMIN_USERNAME,
                [UserField.ROLE]: UserRole[UserRole.ADMIN]
            });
        });

        it("/users/:username should return undefined if the user doesn't exist", async() => {
            const response = await axios.get<ApiResponse<UserFetchResponseType>>(url("test"), AXIOS_CONFIG);
            expect(response.status).to.eq(200);

            // Assert response
            const responseData = response.data;
            expect(responseData).to.be.not.null.and.to.be.not.undefined;
            expect(responseData.error).to.be.null;
            expect(responseData.data).to.be.null;
        });
    });
});
