/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { beforeEach, describe, it } from "mocha";
import axios, { AxiosRequestConfig } from "axios";
import config from "../../../../main/config/service";
import { UserCreationInfo, UserField, UserView } from "../../../../main/model/user";
import { UserRole } from "../../../../main/model/role";
import PasswordHashService from "../../../../main/services/implementations/hash_password";
import { ApiResponse } from "@rpga/common";
import { expect } from "chai";
import SERVICES from "../../../utils/services";
import { UserService, UserServiceErrorReason } from "../../../../main/services/implementations/user";

const AXIOS_CONFIG: AxiosRequestConfig = { validateStatus: () => true };

function parameters(username: string | undefined = undefined, role: string | undefined = undefined, offset: string | undefined = undefined, limit: string | undefined = undefined) {
    return [ [ UserField.USERNAME, username ], [ UserField.ROLE, role ], [ UserService.OFFSET_QUERY_PARAMETER_NAME, offset ], [ UserService.LIMIT_QUERY_PARAMETER_NAME, limit ] ]
        .filter(nameAndValue => nameAndValue[1] !== undefined)
        .map((nameAndValue => `${nameAndValue[0]}=${nameAndValue[1]}`))
        .join("&");
}

function url(username: string | undefined = undefined, role: string | undefined = undefined, offset: string | undefined = undefined, limit: string | undefined = undefined) {
    return `http://localhost:${config.http.port}/users?${parameters(username, role, offset, limit)}`;
}

const PASSWORD = new PasswordHashService().hashSync("test");
const USERS_TO_CREATE = [
    UserCreationInfo.fromFields("fifthadmin", UserRole[UserRole.ADMIN], PASSWORD),
    UserCreationInfo.fromFields("fifthuser", UserRole[UserRole.USER], PASSWORD),

    UserCreationInfo.fromFields("firstadmin", UserRole[UserRole.ADMIN], PASSWORD),
    UserCreationInfo.fromFields("firstuser", UserRole[UserRole.USER], PASSWORD),

    UserCreationInfo.fromFields("fourthUser", UserRole[UserRole.USER], PASSWORD),
    UserCreationInfo.fromFields("fourthadmin", UserRole[UserRole.ADMIN], PASSWORD),

    UserCreationInfo.fromFields("secondadmin", UserRole[UserRole.ADMIN], PASSWORD),
    UserCreationInfo.fromFields("seconduser", UserRole[UserRole.USER], PASSWORD),

    UserCreationInfo.fromFields("sixthadmin", UserRole[UserRole.ADMIN], PASSWORD),
    UserCreationInfo.fromFields("sixthuser", UserRole[UserRole.USER], PASSWORD),

    UserCreationInfo.fromFields("thirdadmin", UserRole[UserRole.ADMIN], PASSWORD),
    UserCreationInfo.fromFields("thirduser", UserRole[UserRole.USER], PASSWORD),
];
const USERS = [ {
    [UserField.USERNAME]: "admin",
    [UserField.ROLE]: UserRole[UserRole.ADMIN]
} ].concat(USERS_TO_CREATE.map(u => u.asView().toJSON()));

describe("API tests", () => {
    describe("/users [GET]", function() {
        beforeEach(async() => {
            for (const info of USERS_TO_CREATE) {
                await SERVICES.userDatabase.insert(info);
            }
        });

        /* TODO: Uncomment when it's implemented
        it("/users should return an error if the request is done by a user that isn't authenticated", async() => {
            const response = await axios.get<ApiResponse<string>>(url(), AXIOS_CONFIG);
            // TODO: Assert http code

            // Assert response
            const responseData = response.data;
            expect(responseData).to.be.not.null.and.to.be.not.undefined;
            expect(responseData.error).to.be.deep.eq("AUTH_NO_TOKEN"); // TODO: Replace by enum value
            expect(responseData.data).to.be.deep.eq("KO");
        });

        it("/users should return an error if the request is done by a basic user", async() => {
            const response = await axios.get<ApiResponse<string>>(url(), AXIOS_CONFIG);
            // TODO: Assert http code

            // Assert response
            const responseData = response.data;
            expect(responseData).to.be.not.null.and.to.be.not.undefined;
            expect(responseData.error).to.be.deep.eq("USER_UNAUTHORIZED"); // TODO: Replace by enum value
            expect(responseData.data).to.be.deep.eq("KO");
        });
        */

        it("If no parameters are specified, /users should return all users", async() => {
            const response = await axios.get<ApiResponse<UserView[]>>(url(), AXIOS_CONFIG);
            expect(response.status).to.eq(200);

            // Assert response
            const responseData = response.data;
            expect(responseData).to.be.not.null.and.to.be.not.undefined;
            expect(responseData.error).to.be.null;
            expect(responseData.data).to.have.same.deep.ordered.members(USERS);
        });

        for (const { queryParameters, users } of [
            { queryParameters: [ "thu" ], users: [ USERS[2], USERS[5], USERS[10] ] },
            { queryParameters: [ undefined, undefined, "5" ], users: USERS.slice(5) },
            { queryParameters: [ undefined, undefined, undefined, "5" ], users: USERS.slice(0, 5) },
            { queryParameters: [ undefined, undefined, "0", "5" ], users: USERS.slice(0, 5) },
            { queryParameters: [ "thu", undefined, "1", "1" ], users: [ USERS[5] ] },
            { queryParameters: [ "fifth" ], users: [ USERS[1], USERS[2] ] },
            { queryParameters: [ "admin", UserRole[UserRole.ADMIN] ], users: USERS.filter(u => u[UserField.ROLE] === UserRole[UserRole.ADMIN]), },
            { queryParameters: [ undefined, UserRole[UserRole.ADMIN] ], users: USERS.filter(u => u[UserField.ROLE] === UserRole[UserRole.ADMIN]), },
            { queryParameters: [ undefined, UserRole[UserRole.USER] ], users: USERS.filter(u => u[UserField.ROLE] === UserRole[UserRole.USER]), }
        ]) {
            it(`/users?${parameters(...queryParameters)} should return: ${users.map(u => u[UserField.USERNAME]).join(", ")}`, async() => {
                const response = await axios.get<ApiResponse<UserView[]>>(url(...queryParameters), AXIOS_CONFIG);
                expect(response.status).to.eq(200);

                // Assert response
                const responseData = response.data;
                expect(responseData).to.be.not.null.and.to.be.not.undefined;
                expect(responseData.error).to.be.null;
                expect(responseData.data).to.have.same.deep.ordered.members(users);
            });
        }

        for (const limit of [ "-1", "0", `${Number.NaN}`, `${Number.POSITIVE_INFINITY}`, "test", `${Number.MAX_SAFE_INTEGER}` ]) {
            const params = [ undefined, undefined, undefined, limit ];

            it(`/users?${parameters(...params)} should return an error`, async() => {
                const response = await axios.get<ApiResponse<UserView[]>>(url(...params), AXIOS_CONFIG);
                expect(response.status).to.eq(500);

                // Assert response
                const responseData = response.data;
                expect(responseData).to.be.not.null.and.to.be.not.undefined;
                expect(responseData.error).to.be.deep.eq(UserServiceErrorReason[UserServiceErrorReason.INVALID_USER_LIST_LIMIT]);
                expect(responseData.data).to.deep.eq("KO");
            });
        }
        for (const offset of [ "-1", `${Number.NaN}`, `${Number.POSITIVE_INFINITY}`, "test" ]) {
            const params = [ undefined, undefined, offset ];

            it(`/users?${parameters(...params)} should return an error`, async() => {
                const response = await axios.get<ApiResponse<UserView[]>>(url(...params), AXIOS_CONFIG);
                expect(response.status).to.eq(500);

                // Assert response
                const responseData = response.data;
                expect(responseData).to.be.not.null.and.to.be.not.undefined;
                expect(responseData.error).to.be.deep.eq(UserServiceErrorReason[UserServiceErrorReason.INVALID_USER_LIST_OFFSET]);
                expect(responseData.data).to.deep.eq("KO");
            });
        }
    });
});
