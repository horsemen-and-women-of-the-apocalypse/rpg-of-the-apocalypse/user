/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { describe, it } from "mocha";
import axios, { AxiosRequestConfig } from "axios";
import { ApiResponse } from "@rpga/common";
import { expect } from "chai";
import config from "../../../../main/config/service";
import Services from "../../../utils/services";
import { UserCreationInfo, UserField } from "../../../../main/model/user";
import { UserRole } from "../../../../main/model/role";
import { UserService, UserServiceErrorReason } from "../../../../main/services/implementations/user";
import PasswordHashService from "../../../../main/services/implementations/hash_password";
import { UserModelErrorReason } from "../../../../main/model/user_error";

const AXIOS_CONFIG: AxiosRequestConfig = { validateStatus: () => true };
const USER_CREATE_INFO = UserCreationInfo.fromFields("test", UserRole[UserRole.USER], new PasswordHashService().hashSync("test"));
const URL = `http://localhost:${config.http.port}/users`;

describe("API tests", () => {
    describe("/users [POST]", function() {
        /* TODO: Uncomment when it's implemented
        it("/users should return an error if the request is done by a user that isn't authenticated", async() => {
            const response = await axios.post<ApiResponse<string>>(URL, {
                [UserField.USERNAME]: USER_CREATE_INFO.username
            }, AXIOS_CONFIG);
            // TODO: Assert http code

            // Assert response
            const responseData = response.data;
            expect(responseData).to.be.not.null.and.to.be.not.undefined;
            expect(responseData.error).to.be.deep.eq("AUTH_NO_TOKEN"); // TODO: Replace by enum value
            expect(responseData.data).to.be.deep.eq("KO");
        });

        it("/users should return an error if the request is done by a basic user", async() => {
            const response = await axios.post<ApiResponse<string>>(URL, {
                [UserField.USERNAME]: USER_CREATE_INFO.username
            }, AXIOS_CONFIG);
            // TODO: Assert http code

            // Assert response
            const responseData = response.data;
            expect(responseData).to.be.not.null.and.to.be.not.undefined;
            expect(responseData.error).to.be.deep.eq("USER_UNAUTHORIZED"); // TODO: Replace by enum value
            expect(responseData.data).to.be.deep.eq("KO");
        });
         */

        it("/users should return an error if the payload is empty", async() => {
            // Create a user with an empty payload
            const response = await axios.post<ApiResponse<string>>(URL, null, AXIOS_CONFIG);
            expect(response.status).to.eq(500);
            const responseData = response.data;

            expect(responseData).to.be.not.null.and.to.be.not.undefined;
            expect(responseData.error).to.be.deep.eq(UserModelErrorReason[UserModelErrorReason.INVALID_USERNAME]);
            expect(responseData.data).to.be.deep.eq("KO");
        });

        it("/users should return an error if the user to create already exists", async() => {
            // Insert a user
            await Services.userDatabase.insert(USER_CREATE_INFO);

            // Create the same user
            const response = await axios.post<ApiResponse<string>>(URL, {
                [UserField.USERNAME]: USER_CREATE_INFO.username
            }, AXIOS_CONFIG);
            expect(response.status).to.eq(500);

            // Assert response
            const responseData = response.data;
            expect(responseData).to.be.not.null.and.to.be.not.undefined;
            expect(responseData.error).to.be.deep.eq(UserServiceErrorReason[UserServiceErrorReason.USER_ALREADY_EXISTS]);
            expect(responseData.data).to.be.deep.eq("KO");
        });

        it("/users should return the password if the user to create is a new user", async() => {
            // Create user
            const response = await axios.post<ApiResponse<string>>(URL, {
                [UserField.USERNAME]: USER_CREATE_INFO.username
            }, AXIOS_CONFIG);
            expect(response.status).to.eq(200);

            // Assert response
            const responseData = response.data;
            expect(responseData).to.be.not.null.and.to.be.not.undefined;
            expect(responseData.error).to.be.null;
            expect(typeof responseData.data).to.be.deep.eq("string");
            expect(responseData.data).to.have.lengthOf(UserService.PASSWORD_LENGTH);

            // Assert user role
            const user = await Services.userDatabase.findByUsername(USER_CREATE_INFO.username);
            expect(user?.role).to.be.deep.eq(UserRole.USER);
        });
    });
});
