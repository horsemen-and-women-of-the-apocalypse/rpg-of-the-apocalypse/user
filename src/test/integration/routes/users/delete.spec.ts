/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { beforeEach, describe, it } from "mocha";
import axios, { AxiosRequestConfig } from "axios";
import config from "../../../../main/config/service";
import { UserCreationInfo, UserField } from "../../../../main/model/user";
import { UserRole } from "../../../../main/model/role";
import PasswordHashService from "../../../../main/services/implementations/hash_password";
import { ApiResponse } from "@rpga/common";
import { expect } from "chai";
import SERVICES from "../../../utils/services";
import amqp, { Connection } from "amqplib";
import {
    EXCHANGE,
    EXCHANGE_PARAMETERS,
    USER_REMOVAL_ROUTING_KEY
} from "../../../../main/services/implementations/rabbitmq_broker";
import { IMessageBroker } from "../../../../main/services/interfaces";

const AXIOS_CONFIG: AxiosRequestConfig = { validateStatus: () => true };
const QUEUES = [ "USER_REMOVAL_QUEUE_0", "USER_REMOVAL_QUEUE_1" ];
const RABBITMQ_URL = config.rabbitmq.url;

function url(username: string) {
    return `http://localhost:${config.http.port}/users/${username}`;
}

async function createConsumer(queue: string): Promise<[Connection, Promise<string>]> {
    const connection = await amqp.connect(config.rabbitmq.url);

    const channel = await connection.createChannel();
    await channel.assertExchange(...EXCHANGE_PARAMETERS);
    const queueAssertion = await channel.assertQueue(queue, {
        durable: true
    });
    await channel.bindQueue(queueAssertion.queue, EXCHANGE, USER_REMOVAL_ROUTING_KEY);

    const consume = new Promise<string>(resolve => {
        channel.consume(queueAssertion.queue, msg => {
            resolve(msg?.content?.toString("utf-8") ?? "{}");
        }, { exclusive: true });
    });

    return [ connection, consume ];
}

const USER = UserCreationInfo.fromFields("test", UserRole[UserRole.USER], new PasswordHashService().hashSync("test"));

describe("API tests", () => {
    describe("/users/:username [DELETE]", () => {
        beforeEach(async() => {
            await SERVICES.userDatabase.insert(USER);

            // Purge queues
            let connection: Connection | undefined = undefined;
            try {
                connection = await amqp.connect(RABBITMQ_URL);
                const channel = await connection.createChannel();
                for (const queue of QUEUES) {
                    await channel.purgeQueue((await channel.assertQueue(queue, { durable: true })).queue);
                }
            } finally {
                connection?.close();
            }
        });

        /* TODO: Uncomment when it's implemented
        it("/users/:username should return an error if the request is done by a user that isn't authenticated", async() => {
            const response = await axios.delete<ApiResponse<string>>(url(USER.username), AXIOS_CONFIG);
            // TODO: Assert http code

            // Assert response
            const responseData = response.data;
            expect(responseData).to.be.not.null.and.to.be.not.undefined;
            expect(responseData.error).to.be.deep.eq("AUTH_NO_TOKEN"); // TODO: Replace by enum value
            expect(responseData.data).to.be.deep.eq("KO");
        });

        it("/users/:username should return an error if the request is done by a basic user", async() => {
            const response = await axios.delete<ApiResponse<string>>(url(USER.username), AXIOS_CONFIG);
            // TODO: Assert http code

            // Assert response
            const responseData = response.data;
            expect(responseData).to.be.not.null.and.to.be.not.undefined;
            expect(responseData.error).to.be.deep.eq("USER_UNAUTHORIZED"); // TODO: Replace by enum value
            expect(responseData.data).to.be.deep.eq("KO");
        });
        */

        it("/users/:username should return true if the user was removed", async() => {
            const consumers: [Connection, Promise<string>][] = [];
            try {
                // Create consumers
                for (const queue of QUEUES) {
                    consumers.push(await createConsumer(queue));
                }

                const response = await axios.delete<ApiResponse<boolean>>(url(USER.username), AXIOS_CONFIG);
                expect(response.status).to.eq(200);

                // Assert response
                const responseData = response.data;
                expect(responseData).to.be.not.null.and.to.be.not.undefined;
                expect(responseData.error).to.be.null;
                expect(responseData.data).to.be.true;

                // Assert notifications
                for (const [ , consume ] of consumers) {
                    expect(JSON.parse(await consume)).to.be.deep.eq({ [UserField.USERNAME]: USER.username });
                }
            } finally {
                for (const [ connection, ] of consumers) {
                    await connection.close();
                }
            }
        });

        it("/users/:username works without message broker", async() => {
            // Mock message broker
            await SERVICES.replaceMessageBroker(new class implements IMessageBroker {
                ping(): Promise<void> {
                    throw new Error("Error for testing purpose");
                }

                notifyUserRemoval(): Promise<void> {
                    throw new Error("Error for testing purpose");
                }
            });

            const response = await axios.delete<ApiResponse<boolean>>(url(USER.username), AXIOS_CONFIG);
            expect(response.status).to.eq(200);

            // Assert response
            const responseData = response.data;
            expect(responseData).to.be.not.null.and.to.be.not.undefined;
            expect(responseData.error).to.be.null;
            expect(responseData.data).to.be.true;
        });

        it("/users/:username should return false if the user doesn't exist", async() => {
            const response = await axios.delete<ApiResponse<boolean>>(url("undefined"), AXIOS_CONFIG);
            expect(response.status).to.eq(200);

            // Assert response
            const responseData = response.data;
            expect(responseData).to.be.not.null.and.to.be.not.undefined;
            expect(responseData.error).to.be.null;
            expect(responseData.data).to.be.false;
        });
    });
});
