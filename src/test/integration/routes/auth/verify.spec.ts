/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { describe, it } from "mocha";
import axios, { AxiosRequestConfig, AxiosRequestHeaders } from "axios";
import config from "../../../../main/config/service";
import { ApiResponse } from "@rpga/common";
import { expect } from "chai";
import { UserCreationInfo } from "../../../../main/model/user";
import { UserRole } from "../../../../main/model/role";
import { AuthInfo, AuthInfoField } from "../../../../main/model/auth_info";
import { AuthServiceErrorReason } from "../../../../main/services/implementations/auth";
import SERVICES from "../../../utils/services";
import PasswordHashService from "../../../../main/services/implementations/hash_password";

const AXIOS_CONFIG: AxiosRequestConfig = { validateStatus: () => true };
const URL = `http://localhost:${config.http.port}/auth/verify`;
const USERNAME = "xXTheKillerXx";
const USER = UserCreationInfo.fromJSON(
    {
        username: USERNAME,
        role: UserRole[UserRole.USER],
        password: new PasswordHashService().hashSync("password")
    });

function authInfo(token: string): AuthInfo {
    return AuthInfo.fromJSON(
        {
            [AuthInfoField.TOKEN]: token,
        }
    );
}

function headers(token: string): AxiosRequestHeaders {
    return {
        Authorization: `${config.userAuth.prefix} ${token}`
    };
}

describe("API tests", () => {
    describe("/auth/verify [GET]", function() {
        it("/auth/verify should return an error if the token is missing", async() => {
            for (const headers of [ { Authorization: config.userAuth.prefix }, { Authorization: "" } ]) {
                const axiosConfig: AxiosRequestConfig =
                    {
                        ...AXIOS_CONFIG,
                        headers: headers
                    };

                const response = await axios.get<ApiResponse<string>>(URL, axiosConfig);
                expect(response.status).to.eq(500);

                // Assert response
                const responseData = response.data;
                expect(responseData).to.be.not.null.and.to.be.not.undefined;
                expect(responseData.error).to.be.deep.eq(AuthServiceErrorReason[AuthServiceErrorReason.AUTH_NO_TOKEN]);
                expect(responseData.data).to.be.deep.eq("KO");
            }

            // No authorization header
            const axiosConfig: AxiosRequestConfig =
                {
                    ...AXIOS_CONFIG,
                    headers: {
                        Accept: "application/json"
                    }
                };

            const response = await axios.get<ApiResponse<string>>(URL, axiosConfig);
            expect(response.status).to.eq(500);

            // Assert response
            const responseData = response.data;
            expect(responseData).to.be.not.null.and.to.be.not.undefined;
            expect(responseData.error).to.be.deep.eq(AuthServiceErrorReason[AuthServiceErrorReason.AUTH_NO_TOKEN]);
            expect(responseData.data).to.be.deep.eq("KO");
        });

        it("/auth/verify should return an error if the token is not linked to some user", async() => {
            // Create user in database
            await SERVICES.userDatabase.insert(USER);

            // Create a token
            const notLinkedToken = SERVICES.auth.tokenSync(USERNAME);

            const axiosConfig: AxiosRequestConfig = {
                ...AXIOS_CONFIG,
                ...{
                    headers: headers(notLinkedToken)
                }
            };

            const response = await axios.get<ApiResponse<string>>(URL, axiosConfig);
            expect(response.status).to.eq(500);

            // Assert response
            const responseData = response.data;
            expect(responseData).to.be.not.null.and.to.be.not.undefined;
            expect(responseData.error).to.be.deep.eq(AuthServiceErrorReason[AuthServiceErrorReason.AUTH_UNKNOWN_TOKEN]);
            expect(responseData.data).to.be.deep.eq("KO");
        });

        it("/auth/verify should return an error if the token has expired", async() => {
            // Create user in database
            await SERVICES.userDatabase.insert(USER);

            // Create token
            const expiredToken = SERVICES.auth.tokenSync(USERNAME, "1000"); // 1s
            await SERVICES.userDatabase.updateAuthInfo(USERNAME, authInfo(expiredToken));

            await new Promise(resolve => setTimeout(resolve, 1100));

            const axiosConfig: AxiosRequestConfig =
                {
                    ...AXIOS_CONFIG,
                    ...{
                        headers: headers(expiredToken)
                    }
                };

            const response = await axios.get<ApiResponse<string>>(URL, axiosConfig);
            expect(response.status).to.eq(500);

            // Assert response
            const responseData = response.data;
            expect(responseData).to.be.not.null.and.to.be.not.undefined;
            expect(responseData.error).to.be.deep.eq(AuthServiceErrorReason[AuthServiceErrorReason.AUTH_EXPIRED_TOKEN]);
            expect(responseData.data).to.be.deep.eq("KO");
        });

        it("/auth/verify should validate a valid token", async() => {
            // Create user in database
            await SERVICES.userDatabase.insert(USER);

            // Set a token to this user
            const token = SERVICES.auth.tokenSync(USERNAME);
            await SERVICES.userDatabase.updateAuthInfo(USERNAME, authInfo(token));

            const axiosConfig: AxiosRequestConfig =
                {
                    ...AXIOS_CONFIG,
                    ...{
                        headers: headers(token)
                    }
                };

            const response = await axios.get<ApiResponse<string>>(URL, axiosConfig);
            expect(response.status).to.eq(200);

            // Assert response
            const responseData = response.data;
            expect(responseData).to.be.not.null.and.not.undefined;
            expect(responseData.data).to.be.deep.eq("OK");
        });
    });
});
