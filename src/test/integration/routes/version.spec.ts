/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { describe } from "mocha";
import axios from "axios";
import config from "../../../main/config/service";
import { ApiResponse } from "@rpga/common";
import { expect } from "chai";

const PORT = config.http.port;

describe("API tests", () => {
    describe("/version", function() {
        it("/version return the version in package.json", async() => {
            const response = await axios.get<ApiResponse<string>>(`http://localhost:${PORT}/version`);
            const responseData = response.data;

            expect(responseData).to.be.not.null.and.to.be.not.undefined;
            expect(responseData.error).to.be.null;
            expect(responseData.data).to.eq(process.env.npm_package_version, "Version received is the same as the one defined in package.json");
        });
    });
});
