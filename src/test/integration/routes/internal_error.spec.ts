/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { describe } from "mocha";
import { ServerError } from "../../../main/server_errors";
import SERVICES from "../../utils/services";
import { IVersionService } from "../../../main/services/interfaces";
import { ApiError, ApiResponse } from "@rpga/common";
import axios from "axios";
import { expect } from "chai";
import config from "../../../main/config/service";

const PORT = config.http.port;

describe("API tests", () => {
    describe("/*", function() {
        it(`If a route doesn't throw a ${ApiError.name}, it will return a ${ServerError[ServerError.INTERNAL_ERROR]}`, async() => {
            // Mock version service
            await SERVICES.replaceVersion(new class implements IVersionService {
                get version(): string {
                    throw new Error("Error for testing purpose");
                }
            });

            const response = await axios.get<ApiResponse<string>>(`http://localhost:${PORT}/version`, { validateStatus: () => true });
            expect(response.status).to.be.eq(500);

            const responseData = response.data;
            expect(responseData).to.be.not.null.and.to.be.not.undefined;
            expect(responseData.error).to.be.deep.eq(ServerError[ServerError.INTERNAL_ERROR]);
            expect(responseData.data).to.be.deep.eq("KO");
        });
    });
});
