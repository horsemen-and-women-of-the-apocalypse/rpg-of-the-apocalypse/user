/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { describe, it } from "mocha";
import { expect } from "chai";
import Services from "../../utils/services";
import { User, UserCreationInfo, UserField } from "../../../main/model/user";
import { UserRole } from "../../../main/model/role";
import UserDatabaseService from "../../../main/services/implementations/user_database";
import { ObjectId, MongoServerError } from "mongodb";
import { AuthService } from "../../../main/services/implementations/auth";
import { AuthInfo, AuthInfoField } from "../../../main/model/auth_info";
import config from "../../../main/config/service";
import PasswordHashService from "../../../main/services/implementations/hash_password";

const userInfo = UserCreationInfo.fromFields("test", UserRole[UserRole.USER], new PasswordHashService().hashSync("test"));

describe("Services", () => {
    describe("IUserDatabaseService", () => {
        it("Users collection is reset before a test", async() => {
            const userDatabase = Services.userDatabase;
            const users = await userDatabase.all();
            expect(users).to.have.lengthOf(1, "Collection contains only the admin");

            // Assert admin
            const admin = users[0];
            expect(admin).to.be.instanceof(User);
            expect(admin.role).to.be.eq(UserRole.ADMIN);
            expect(await admin.hasPassword(config.userAuth.adminPassword, Services.hash)).to.be.true;

            // Assert unique username index
            const usernameIndex = (await (await userDatabase.collection()).indices()).filter(i => i.name.includes(UserField.USERNAME))[0];
            expect(usernameIndex).to.be.not.null.and.not.undefined;
            expect(usernameIndex.unique).to.be.true;
        });

        it("Administrator isn't created if it already exists", async() => {
            // Find admin
            const oldAdmin = await Services.userDatabase.findByUsername(UserDatabaseService.ADMIN_USERNAME);

            // Reset services so that the administrator creation should be triggered
            await Services.reset();

            // Find admin
            const newAdmin = await Services.userDatabase.findByUsername(UserDatabaseService.ADMIN_USERNAME);

            expect(newAdmin).to.be.deep.eq(oldAdmin);
        });

        describe("IUserDatabaseService#all", () => {
            it("all return only valid users", async() => {
                const service = Services.userDatabase;
                const serviceCollection = await Services.database.collection(UserDatabaseService.COLLECTION);

                // Save users
                await service.insert(userInfo);
                await serviceCollection.insert({ [UserField.USERNAME]: "test_2" });

                expect(await serviceCollection.all().toArray()).to.have.lengthOf(3, "There are 2 users and the admin");
                const users = await service.all();
                expect(users).to.have.lengthOf(2, "There are only two valid users");
            });
        });

        describe("IUserDatabaseService#findById", () => {
            it("findById return the user if it exists or null if not", async() => {
                const service = Services.userDatabase;

                const id = await service.insert(userInfo);
                const user = await service.findById(id);
                expect(user).to.be.instanceof(User, "User exists");
                expect(user?.id).to.deep.eq(id, "User has the same id as the saved one");

                expect(user?.isAuthInfoTokenEqual("")).to.be.false;

                expect(await service.findById(new ObjectId())).to.be.null;
            });
        });

        describe("IUserDatabaseService#findByUsername", () => {
            it("findByUsername return the user if it exists or null if not", async() => {
                const service = Services.userDatabase;
                const refUsername = userInfo.username;

                await service.insert(userInfo);
                const user = await service.findByUsername(refUsername);
                expect(user).to.be.instanceof(User, "User exist");
                expect(user?.username).to.deep.eq(refUsername, "User has the same username as the saved one");

                expect(await service.findByUsername("unknown_user")).to.be.null;
            });
        });

        describe("IUserDatabaseService#insert", () => {
            it("insert throw an error when inserting a duplicate user", async() => {
                const service = Services.userDatabase;

                await service.insert(userInfo);
                await expect(service.insert(userInfo))
                    .to.be.rejectedWith(MongoServerError, "E11000", "Inserting a user with an already existing username should throw an error");
            });
        });

        describe("IUserDatabaseService#updateAuthInfo", async() => {
            const authService = new AuthService(Services.userDatabase);
            const refUsername = userInfo.username;
            const token = authService.tokenSync(refUsername);

            const authInfo = AuthInfo.fromJSON({
                [AuthInfoField.TOKEN]: token
            });

            it("updateAuthInfo can update user auth info field based on its id", async() => {
                const userDbService = Services.userDatabase;
                const id = await userDbService.insert(userInfo);
                await userDbService.updateAuthInfo(id, authInfo);
                const updatedUser = await userDbService.findById(id);

                expect(updatedUser?.toJSON()[UserField.AUTH_INFO]).to.be.deep.eq(authInfo.toJSON());
                expect(updatedUser?.isAuthInfoTokenEqual(token)).to.be.true;
            });

            it("updateAuthInfo can update user auth info field based on its username", async() => {
                const userDbService = Services.userDatabase;
                await userDbService.insert(userInfo);
                await userDbService.updateAuthInfo(refUsername, authInfo);
                const updatedUser = await userDbService.findByUsername(refUsername);

                expect(updatedUser?.toJSON()[UserField.AUTH_INFO]).to.be.deep.eq(authInfo.toJSON());
                expect(updatedUser?.isAuthInfoTokenEqual(token)).to.be.true;
            });

            it("Update a non-existent user should log an error", async() => {
                await Services.userDatabase.updateAuthInfo(refUsername, authInfo);
            });
        });

        describe("IUserDatabaseService#remove", () => {
            it("remove can remove a user based on its id or its username", async() => {
                const service = Services.userDatabase;
                const refUsername = userInfo.username;

                // Based on id
                const id = await service.insert(userInfo);
                expect(await service.findByUsername(refUsername)).to.be.instanceof(User, "User exists");
                expect(await service.remove(id)).to.be.true;
                expect(await service.findByUsername(refUsername)).to.be.null;

                // Based on username
                await service.insert(userInfo);
                expect(await service.findByUsername(refUsername)).to.be.instanceof(User, "User exists");
                expect(await service.remove(refUsername)).to.be.true;
                expect(await service.findByUsername(refUsername)).to.be.null;

                // The user doesn't exist
                expect(await service.remove(refUsername)).to.be.false;
            });
        });
    });
});
