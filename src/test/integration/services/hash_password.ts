/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { describe, it } from "mocha";
import PasswordHashService from "../../../main/services/implementations/hash_password";
import { expect } from "chai";

const SERVICE = new PasswordHashService();
const LONG_PASSWORD = Buffer.alloc(73).toString();
const PASSWORD = "test";

describe("Services", () => {
    describe("PasswordHashService", () => {
        for (const { hash } of [
            { hash: SERVICE.hash  },
            { hash: SERVICE.hashSync }
        ]) {
            it(`PasswordHashService#${hash.name} should throw an error if password exceeds 72 bytes`, () => {
                expect(() => hash(LONG_PASSWORD)).to.throw(Error, "Password is too long");
            });

            it(`PasswordHashService#${hash.name} should return a hash if the password is valid`, async() => {
                const res = await hash(PASSWORD);
                expect(res).to.have.lengthOf(60);
            });
        }
    });
});
