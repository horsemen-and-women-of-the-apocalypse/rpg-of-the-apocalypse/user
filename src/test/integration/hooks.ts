/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import "reflect-metadata";
import { after, before } from "mocha";
import { ExpressServer } from "../../main/server";
import Services from "../utils/services";
import chaiAsPromised from "chai-as-promised";
import chai from "chai";

// Add promise support to chai
chai.use(chaiAsPromised);

let server: ExpressServer | null = null;
before(async function() {
    // Extend timeout because the server doesn't start instantly
    this.timeout(5_000);

    // Create and start server
    server = ExpressServer.create();
    await server.start();
});

// Reset database/services before each test
beforeEach(async() => {
    await Services.reset();
    await Services.database.dropCollections();
});

// Close server after integrations tests
after(done => {
    const srv = server;
    if (srv !== null) {
        srv.close(done);
    } else {
        done();
    }
});
