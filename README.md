# User service

<p align="center">A REST API exposing endpoints for users management</p>

## Requirements

* NodeJS (version: `16.13.0`)
* MongoDB (version: `4.4`)
* RabbitMQ (version: `3.9.15`)

## API documentation

The documentation of routes is defined by `http://{url}/docs/`

## Set-up for development

### Install dependencies

```
npm install
```

### Create configuration

Create a configuration `app.development.ini` in `config` based on the template configuration.

### Run

```
npm run start:dev
```

Server will automatically restart when a change in files is detected.

## Common commands

### Tests

Create a configuration `app.test.ini` in `config` based on the template configuration.

```
npm run test
```

### Code style

`npm run lint` to check code style and `npm run lint:fix` to fix it.

### Keys for generating JWTs in user authentication

Create a pair of private/public RSA keys in a new directory (which matches the path set in the configuration file) to sign JSON Web Tokens :

```bash
ssh-keygen -t rsa -b 1024 -m PEM -f ./rpga_rsa.key
openssl rsa -in ./rpga_rsa.key -pubout -outform PEM -out ./rpga_rsa.key.pub
```
